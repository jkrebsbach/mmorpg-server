﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using NewMoonSolutions.Models;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;

namespace NewMoonSolutions.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private ApplicationUserManager _userManager;

        public AccountController()
        {
        }

        public AccountController(ApplicationUserManager userManager, ApplicationSignInManager signInManager )
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        private ApplicationSignInManager _signInManager;

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set { _signInManager = value; }
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // This doesn't count login failures towards account lockout
            // To enable password failures to trigger account lockout, change to shouldLockout: true
            var result = await SignInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, shouldLockout: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = model.RememberMe });
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid login attempt.");
                    return View(model);
            }
        }

        [AllowAnonymous]
        public JsonResult ValidateHash(string email, string hash)
        {
            string token = string.Empty;
            bool success = false;
            SignInStatus result = SignInStatus.Failure;

            using (skyfishEntities entities = new skyfishEntities())
            {
                success = entities.AspNetUsers.Any(u => u.Email == email && u.PasswordHash == hash);
            }

            if (success)
            {
                result = SignInStatus.Success;
                LoginUser(email, out token, out hash);
            }

            Dictionary<string, string> model = new Dictionary<string, string>();
            model["Success"] = success.ToString();
            model["Status"] = ((int)result).ToString();
            model["CurrentTime"] = DateTime.UtcNow.Ticks.ToString();
            model["Token"] = token;
            model["Hash"] = hash;

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        public ActionResult ThirdPartySuccess()
        {
            return View();
        }

        [AllowAnonymous]
        public async Task<JsonResult> LoginExternal(string email, string password)
        {
            string token = string.Empty;
            string hash = string.Empty;
            bool success = false;


            // This doesn't count login failures towards account lockout
            // To enable password failures to trigger account lockout, change to shouldLockout: true
            var result = await SignInManager.PasswordSignInAsync(email, password, false, shouldLockout: false);
            switch (result)
            {
                case SignInStatus.Success:
                    success = true;
                    LoginUser(email, out token, out hash);
                    break;
                case SignInStatus.LockedOut:
                    token = "Locked";
                    break;
                case SignInStatus.RequiresVerification:
                    token = "SendCode";
                    break;
                case SignInStatus.Failure:
                default:
                    token = "Invalid login attempt.";
                    break;
            }

            Dictionary<string, string> model = new Dictionary<string, string>();
            model["Success"] = success.ToString();
            model["Status"] = ((int)result).ToString();
            model["CurrentTime"] = DateTime.UtcNow.Ticks.ToString();
            model["Token"] = token;
            model["Hash"] = hash;

            HttpContext.Response.AppendHeader("Access-Control-Allow-Origin", "*");

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        public async Task<JsonResult> CheckThirdParty(string guid)
        {
            string token = string.Empty;
            string hash = string.Empty;
            string email = string.Empty;
            string userId = string.Empty;
            bool success = false;

            Guid thirdPartyToken = Guid.Parse(guid);

            SignInStatus result = SignInStatus.Failure;
            ThirdPartyLogin thirdPartyLogin;
            using (skyfishEntities entities = new skyfishEntities())
            {
                thirdPartyLogin =
                    entities.ThirdPartyLogins.FirstOrDefault(u => u.Token == thirdPartyToken);


                if (thirdPartyLogin != null)
                {
                    success = true;
                    result = SignInStatus.Success;
                    email = thirdPartyLogin.Email;

                    userId = entities.AspNetUsers.First(u => u.Email == email).Id;

                    LoginUser(thirdPartyLogin.Email, out token, out hash);
                }
            }

            Dictionary<string, string> model = new Dictionary<string, string>();
            model["Success"] = success.ToString();
            model["Status"] = ((int)result).ToString();
            model["Email"] = email;
            model["User"] = userId;
            model["CurrentTime"] = DateTime.UtcNow.Ticks.ToString();
            model["Token"] = token;
            model["Hash"] = hash;

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        private void ThirdPartyLogin(string email, Guid token)
        {
            using (skyfishEntities entities = new skyfishEntities())
            {
                entities.ThirdPartyUserLogin(email, token);
            }
        }

        private void LoginUser(string email, out string tokenResult, out string hashResult)
        {
            var token = new ObjectParameter("Token", typeof(Guid));
            var hash = new ObjectParameter("Hash", typeof(string));

            using (skyfishEntities entities = new skyfishEntities())
            {
                entities.LoginUser(email, token, hash);
            }

            tokenResult = ((Guid)token.Value).ToString();
            hashResult = hash.Value == DBNull.Value ? null : (string)hash.Value;
        }

        //
        // GET: /Account/VerifyCode
        [AllowAnonymous]
        public async Task<ActionResult> VerifyCode(string provider, string returnUrl, bool rememberMe)
        {
            // Require that the user has already logged in via username/password or external login
            if (!await SignInManager.HasBeenVerifiedAsync())
            {
                return View("Error");
            }
            var user = await UserManager.FindByIdAsync(await SignInManager.GetVerifiedUserIdAsync());
            if (user != null)
            {
                var code = await UserManager.GenerateTwoFactorTokenAsync(user.Id, provider);
            }
            return View(new VerifyCodeViewModel { Provider = provider, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/VerifyCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyCode(VerifyCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // The following code protects for brute force attacks against the two factor codes. 
            // If a user enters incorrect codes for a specified amount of time then the user account 
            // will be locked out for a specified amount of time. 
            // You can configure the account lockout settings in IdentityConfig
            var result = await SignInManager.TwoFactorSignInAsync(model.Provider, model.Code, isPersistent:  model.RememberMe, rememberBrowser: model.RememberBrowser);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(model.ReturnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid code.");
                    return View(model);
            }
        }

        //
        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult Register()
        {
            return View(new RegisterViewModel());
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                using (skyfishEntities entities = new skyfishEntities())
                {
                    var dupCharacter = entities.Characters.FirstOrDefault(p => p.CharacterName == model.CharacterName);
                    if (dupCharacter != null)
                    {
                        ModelState.AddModelError("CHARACTERNAME", "Character name already in use");
                    }
                    else
                    {
                        var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                        var result = await UserManager.CreateAsync(user, model.Password);
                        if (result.Succeeded)
                        {
                            CreateCharacter(entities, user.Id, model.CharacterName, model.CharacterTypeID);
                            
                            await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);

                            // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                            // Send an email with this link
                            // string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                            // var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                            // await UserManager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");
                            return RedirectToAction("Index", "Home");
                        }

                        AddErrors(result);
                    }
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        private void CreateCharacter(skyfishEntities entities, string userID, string characterName, int characterTypeID)
        {
            Character character = new Character()
            {
                UserID = userID,
                CharacterName = characterName,
                CharacterTypeID = characterTypeID
            };

            var newStats = CharacterModel.InitializeCharacter(character.CharacterTypeID);

            character.MaxHealth = newStats.MaxHealth;
            character.MaxEnergy = newStats.MaxEnergy;
            character.Defense = newStats.Defense;
            character.Strength = newStats.Strength;
            character.Agility = newStats.Agility;
            character.Charisma = newStats.Charisma;
            character.Willpower = newStats.Willpower;
            character.Intelligence = newStats.Intelligence;
            character.SkillPoints = newStats.SkillPoints;

            character.EquipBody = null;
            character.EquipHead = null;
            character.EquipLeftHand = null;
            character.EquipRightHand = null;

            character.Gold = 0;
            character.Experience = 0;
            character.Level = 1;

            // Initial inventory
            foreach (var newInventory in newStats.Inventory)
            {
                character.CharacterInventories.Add(
                    new CharacterInventory()
                    {
                        ItemID = newInventory.ItemID,
                        Quantity = newInventory.Quantity
                    });
            }

            entities.Characters.Add(character);
            entities.SaveChanges();
        }

        //
        // GET: /Account/ConfirmEmail
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return View("Error");
            }
            var result = await UserManager.ConfirmEmailAsync(userId, code);
            return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }

        //
        // GET: /Account/ForgotPassword
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        //
        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByNameAsync(model.Email);
                if (user == null || !(await UserManager.IsEmailConfirmedAsync(user.Id)))
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    return View("ForgotPasswordConfirmation");
                }

                // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                // Send an email with this link
                // string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                // var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);		
                // await UserManager.SendEmailAsync(user.Id, "Reset Password", "Please reset your password by clicking <a href=\"" + callbackUrl + "\">here</a>");
                // return RedirectToAction("ForgotPasswordConfirmation", "Account");
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ForgotPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        //
        // GET: /Account/ResetPassword
        [AllowAnonymous]
        public ActionResult ResetPassword(string code)
        {
            return code == null ? View("Error") : View();
        }

        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await UserManager.FindByNameAsync(model.Email);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            var result = await UserManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            AddErrors(result);
            return View();
        }

        //
        // GET: /Account/ResetPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            Guid guid = Guid.NewGuid();

            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl, Guid = guid }));
        }

        public ActionResult GoogleExternalSignOn(string abc)
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult UnityExternalLogin(string provider, string returnUrl, string guid)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl, Guid = guid }));
        }

        //
        // GET: /Account/SendCode
        [AllowAnonymous]
        public async Task<ActionResult> SendCode(string returnUrl, bool rememberMe)
        {
            var userId = await SignInManager.GetVerifiedUserIdAsync();
            if (userId == null)
            {
                return View("Error");
            }
            var userFactors = await UserManager.GetValidTwoFactorProvidersAsync(userId);
            var factorOptions = userFactors.Select(purpose => new SelectListItem { Text = purpose, Value = purpose }).ToList();
            return View(new SendCodeViewModel { Providers = factorOptions, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/SendCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SendCode(SendCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            // Generate the token and send it
            if (!await SignInManager.SendTwoFactorCodeAsync(model.SelectedProvider))
            {
                return View("Error");
            }
            return RedirectToAction("VerifyCode", new { Provider = model.SelectedProvider, ReturnUrl = model.ReturnUrl, RememberMe = model.RememberMe });
        }

        //
        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl, string guid)
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }

            // Sign in the user with this external login provider if the user already has a login
            var result = await SignInManager.ExternalSignInAsync(loginInfo, isPersistent: false);
            switch (result)
            {
                case SignInStatus.Success:
                    var token = Guid.Parse(guid);
                    ThirdPartyLogin(loginInfo.Email, token);
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = false });
                case SignInStatus.Failure:
                default:
                    // If the user does not have an account, then prompt the user to create an account
                    ViewBag.ReturnUrl = returnUrl;
                    ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
                    TempData["LoginProvider"] = loginInfo.Login.LoginProvider;
                    return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { Email = loginInfo.Email });
            }
        }

        //
        // POST: /Account/ExternalLoginConfirmation
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Manage");
            }

            ViewBag.LoginProvider = TempData["LoginProvider"];
            if (ModelState.IsValid)
            {
                using (skyfishEntities entities = new skyfishEntities())
                {
                    var dupCharacter = entities.Characters.FirstOrDefault(p => p.CharacterName == model.CharacterName);
                    if (dupCharacter != null)
                    {
                        ModelState.AddModelError("CHARACTERNAME", "Character name already in use");
                    }
                    else
                    {
                        // Get the information about the user from the external login provider
                        var info = await AuthenticationManager.GetExternalLoginInfoAsync();
                        if (info == null)
                        {
                            return View("ExternalLoginFailure");
                        }
                        var user = new ApplicationUser {UserName = model.Email, Email = model.Email};
                        var result = await UserManager.CreateAsync(user);
                        if (result.Succeeded)
                        {
                            CreateCharacter(entities, user.Id, model.CharacterName, model.CharacterTypeID);

                            result = await UserManager.AddLoginAsync(user.Id, info.Login);
                            if (result.Succeeded)
                            {
                                await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                                return RedirectToLocal(returnUrl);
                            }
                        }
                        AddErrors(result);
                    }
                }
            }

            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        //
        // POST: /Account/LogOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("Index", "Home");
        }

        //
        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion
    }
}