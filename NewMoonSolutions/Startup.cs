﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(NewMoonSolutions.Startup))]
namespace NewMoonSolutions
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
