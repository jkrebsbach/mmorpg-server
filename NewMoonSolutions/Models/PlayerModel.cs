﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NewMoonSolutions.Models
{
    public class PlayerModel
    {
        public string PlayerName { get; set; }
        public List<string> PlayerWarehouse { get; set; }

        public PlayerModel()
        {
        }

        public PlayerModel(Player player)
            : this()
        {
            PlayerWarehouse = new List<string>();

            if (player == null || player.AspNetUser == null)
                PlayerName = string.Empty;
            else
                PlayerName = player.AspNetUser.Email;
        }
    }
}