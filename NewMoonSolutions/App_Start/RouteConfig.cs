﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace NewMoonSolutions
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.IgnoreRoute("Content/WebGL/Release/*");

            routes.MapRoute(
                name: "staticFileRoute",
                url: ".well-known/acme-challenge/kU6pibXgTv14ueIBTqtv4kVJ_FCvuMoUALYYkLNdN00",
                defaults: new { controller = "Acme", action = "Index"});
            routes.MapRoute(
                name: "staticFileRoute2",
                url: ".well-known/acme-challenge/k8dzwTzW4Uj5FN0Fbirsz-zND2uuz4ONlIa1OoEvQQs",
                defaults: new { controller = "Acme", action = "Index2" });

            routes.MapRoute(
                name: "Android",
                url: "android",
                defaults: new { controller = "Account", action = "ThirdPartySuccess", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "iOS",
                url: "iphone",
                defaults: new { controller = "Account", action = "ThirdPartySuccess", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
