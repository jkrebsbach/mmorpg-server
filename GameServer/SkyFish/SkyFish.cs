﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using GameServer.Entities;

namespace GameServer
{
    public class SkyFish
    {
        private readonly string _sqlConnString;

        public SkyFish()
        {
            _sqlConnString = ConfigurationManager.ConnectionStrings["skyfishConnection"].ConnectionString;
        }

        public AuthenticationResult CheckLogin(string email, Guid token)
        {
            var result = new AuthenticationResult()
                {
                    Authenticated = false
                };
            var dsResult = new DataSet();

            using (var sqlConn = new SqlConnection(_sqlConnString))
            using (var cmd = new SqlCommand("CheckLogin", sqlConn))
            {
                sqlConn.Open();
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("Email", email));
                cmd.Parameters.Add(new SqlParameter("Token", token));

                var da = new SqlDataAdapter(cmd);
                da.Fill(dsResult);

                sqlConn.Close();
            }

            if (dsResult.Tables.Count < 1 || dsResult.Tables[0].Rows.Count < 1)
                return result;

            result.Authenticated = (int)dsResult.Tables[0].Rows[0]["Authenticated"] == 1;

            if (!result.Authenticated)
                return result;

            result.UserID = (string)dsResult.Tables[0].Rows[0]["UserID"];
            result.Gold = (int)dsResult.Tables[0].Rows[0]["Gold"];
            result.Experience = (int)dsResult.Tables[0].Rows[0]["Experience"];
            result.Level = (int)dsResult.Tables[0].Rows[0]["Level"];
            result.CharacterID = (int)dsResult.Tables[0].Rows[0]["CharacterID"];

            return result;
        }

        public CharacterModel RetrieveCharacter(int characterID)
        {
            var result = new CharacterModel()
            {
                CharacterID = characterID
            };
            var dsResult = new DataSet();

            using (var sqlConn = new SqlConnection(_sqlConnString))
            using (var cmd = new SqlCommand("RetrieveCharacter", sqlConn))
            {
                sqlConn.Open();
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("CharacterID", characterID));
                
                var da = new SqlDataAdapter(cmd);
                da.Fill(dsResult);

                sqlConn.Close();
            }

            if (dsResult.Tables.Count < 3 || dsResult.Tables[0].Rows.Count < 1)
                return result;

            result.CharacterID = (int)dsResult.Tables[0].Rows[0]["CharacterID"];
            result.CharacterName = (string)dsResult.Tables[0].Rows[0]["CharacterName"];
            result.CharacterTypeID = (int)dsResult.Tables[0].Rows[0]["CharacterTypeID"];
            result.MaxHealth = (int)dsResult.Tables[0].Rows[0]["MaxHealth"];
            result.MaxEnergy = (int)dsResult.Tables[0].Rows[0]["MaxEnergy"];
            result.EquipLeftHand = dsResult.Tables[0].Rows[0]["EquipLeftHand"] == DBNull.Value ? 0 : (int)dsResult.Tables[0].Rows[0]["EquipLeftHand"];
            result.EquipRightHand = dsResult.Tables[0].Rows[0]["EquipRightHand"] == DBNull.Value ? 0 : (int)dsResult.Tables[0].Rows[0]["EquipRightHand"];
            result.EquipHead = dsResult.Tables[0].Rows[0]["EquipHead"] == DBNull.Value ? 0 : (int)dsResult.Tables[0].Rows[0]["EquipHead"];
            result.EquipBody = dsResult.Tables[0].Rows[0]["EquipBody"] == DBNull.Value ? 0 : (int)dsResult.Tables[0].Rows[0]["EquipBody"];
            result.EquipRanged = dsResult.Tables[0].Rows[0]["EquipRanged"] == DBNull.Value ? 0 : (int)dsResult.Tables[0].Rows[0]["EquipRanged"];

            result.Defense = (double)dsResult.Tables[0].Rows[0]["Defense"];
            result.Strength = (double)dsResult.Tables[0].Rows[0]["Strength"];
            result.Agility = (double)dsResult.Tables[0].Rows[0]["Agility"];
            result.Charisma = (double)dsResult.Tables[0].Rows[0]["Charisma"];
            result.Willpower = (double)dsResult.Tables[0].Rows[0]["Willpower"];
            result.Intelligence = (double)dsResult.Tables[0].Rows[0]["Intelligence"];
            result.SkillPoints = (int)dsResult.Tables[0].Rows[0]["SkillPoints"];
            result.FrontRow = (bool)dsResult.Tables[0].Rows[0]["FrontRow"];

            result.LocationID = dsResult.Tables[0].Rows[0]["LocationID"] == DBNull.Value ? null : (int?)dsResult.Tables[0].Rows[0]["LocationID"];
            result.SceneID = dsResult.Tables[0].Rows[0]["SceneID"] == DBNull.Value ? null : (int?)dsResult.Tables[0].Rows[0]["SceneID"];
            result.PlayerX = dsResult.Tables[0].Rows[0]["PlayerX"] == DBNull.Value ? null : (double?)dsResult.Tables[0].Rows[0]["PlayerX"];
            result.PlayerY = dsResult.Tables[0].Rows[0]["PlayerY"] == DBNull.Value ? null : (double?)dsResult.Tables[0].Rows[0]["PlayerY"];

            foreach(DataRow drInventory in dsResult.Tables[1].Rows)
            {
                int itemID = (int)drInventory["ItemID"];
                int quantity = (int)drInventory["Quantity"];

                result.Inventory[itemID] = quantity;
            }

            foreach (DataRow drAbility in dsResult.Tables[2].Rows)
            {
                int abilityID = (int)drAbility["AbilityID"];
                int level = (int)drAbility["AbilityLevel"];

                result.Abilities.Add(abilityID, level);
            }

            foreach (DataRow drQuest in dsResult.Tables[3].Rows)
            {
                var questID = (int)drQuest["QuestID"];
                var questStatus = (Quest.QuestStatusEnum)(int)drQuest["Status"];

                result.PriorQuests[questID] = questStatus;
            }

            foreach (DataRow drQuest in dsResult.Tables[4].Rows)
            {
                var questID = (int)drQuest["QuestID"];
                var enemyID = (int)drQuest["EnemyID"];
                var quantity = (int)drQuest["Quantity"];

                result.QuestKillCounts.Add(new QuestKillCount()
                    {
                        QuestID = questID,
                        CharacterID = characterID,
                        EnemyID = enemyID,
                        Quantity = quantity
                    });
            }

            foreach (DataRow drQuest in dsResult.Tables[5].Rows)
            {
                var questID = (int)drQuest["QuestID"];
                var npcID = (int)drQuest["NPCID"];
                var quantity = (int)drQuest["Quantity"];

                result.QuestTalkCounts.Add(new QuestTalkCount()
                {
                    QuestID = questID,
                    CharacterID = characterID,
                    NPCID = npcID,
                    Quantity = quantity
                });
            }

            return result;
        }

        public List<Store> GetStores(int npcID)
        {
            var result = new List<Store>();


            using (var sqlConn = new SqlConnection(_sqlConnString))
            using (var cmd = new SqlCommand("SELECT StoreID, NPCID FROM Stores WHERE NPCID = @NPCID", sqlConn))
            {
                sqlConn.Open();
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add(new SqlParameter("NPCID", npcID));

                var dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    result.Add(new Store()
                    {
                        StoreID = (int)dr["StoreID"],
                        NPCID = (int)dr["NPCID"]
                    });
                }

                sqlConn.Close();
            }

            return result;
        }

        public List<StoreInventory> GetStoreInventory(int storeID, int npcID)
        {
            List<StoreInventory> result = new List<StoreInventory>();


            using (SqlConnection sqlConn = new SqlConnection(_sqlConnString))
            using (SqlCommand cmd = new SqlCommand(@"SELECT ItemID, Cost 
                FROM StoreInventory si 
                JOIN Stores s ON si.StoreID = s.StoreID
                WHERE s.StoreID = @StoreID AND s.NPCID = @NPCID", sqlConn))
            {
                sqlConn.Open();
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add(new SqlParameter("StoreID", storeID));
                cmd.Parameters.Add(new SqlParameter("NPCID", npcID));

                var dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    result.Add(new StoreInventory()
                        {
                            ItemID = (int)dr["ItemID"],
                            Cost = (int)dr["Cost"]
                        });
                }

                sqlConn.Close();
            }

            return result;
        }

        public List<Quest> GetCharacterQuests(int characterID, Dictionary<int, Quest> questLibrary)
        {
            var result = new List<Quest>();


            using (var sqlConn = new SqlConnection(_sqlConnString))
            using (var cmd = new SqlCommand(@"SELECT Quests.QuestID, Description, NPCID, Status " +
                " FROM Quests INNER JOIN CharacterQuests ON Quests.QuestID = CharacterQuests.QuestID AND CharacterQuests.CharacterID = @CharacterID ", sqlConn))
            {
                sqlConn.Open();
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add(new SqlParameter("CharacterID", characterID));

                var dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    var quest = new Quest()
                    {
                        QuestID = (int)dr["QuestID"],
                        Description = (string)dr["Description"],
                        Status = dr["Status"] == DBNull.Value ? Quest.QuestStatusEnum.NotStarted : (Quest.QuestStatusEnum)dr["Status"]
                    };
                    result.Add(quest);

                    if (questLibrary.ContainsKey(quest.QuestID))
                    {
                        quest.QuestRewards = questLibrary[quest.QuestID].QuestRewards;
                        quest.QuestCondition = questLibrary[quest.QuestID].QuestCondition;
                        quest.QuestRequirements = questLibrary[quest.QuestID].QuestRequirements;
                    }
                }

                sqlConn.Close();
            }

            return result;
        }

        public List<Quest> GetQuests(int npcID, int characterID, Dictionary<int, Quest> questLibrary)
        {
            var result = new List<Quest>();


            using (var sqlConn = new SqlConnection(_sqlConnString))
            using (var cmd = new SqlCommand(@"SELECT Quests.QuestID, Description, NPCID, Status " +
                " FROM Quests LEFT JOIN CharacterQuests ON Quests.QuestID = CharacterQuests.QuestID AND CharacterQuests.CharacterID = @CharacterID " +
                " WHERE NPCID = @NPCID", sqlConn))
            {
                sqlConn.Open();
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add(new SqlParameter("NPCID", npcID));
                cmd.Parameters.Add(new SqlParameter("CharacterID", characterID));

                var dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    var quest = new Quest()
                    {
                        QuestID = (int)dr["QuestID"],
                        Description = (string)dr["Description"],
                        Status = dr["Status"] == DBNull.Value ? Quest.QuestStatusEnum.NotStarted : (Quest.QuestStatusEnum)dr["Status"]
                    };

                    result.Add(quest);

                    if (questLibrary.ContainsKey(quest.QuestID))
                    {
                        quest.QuestRewards = questLibrary[quest.QuestID].QuestRewards;
                        quest.QuestCondition = questLibrary[quest.QuestID].QuestCondition;
                        quest.QuestRequirements = questLibrary[quest.QuestID].QuestRequirements;
                    }
                }

                sqlConn.Close();
            }

            return result;
        }

        internal void UpdateCharacter(Player unit)
        {
            using (var sqlConn = new SqlConnection(_sqlConnString))
            using (var cmd = new SqlCommand(@"UPDATE Characters SET Gold = @Gold, Experience = @Experience, Level = @Level, SkillPoints = @SkillPoints,
                EquipLeftHand = @LeftHand, EquipRightHand = @RightHand, EquipHead = @Head, EquipBody = @Body, EquipRanged=@Ranged, FrontRow = @FrontRow,
                MaxHealth=@MaxHealth,MaxEnergy=@MaxEnergy,
                Defense=@Defense,Strength=@Strength,Agility=@Agility,Charisma=@Charisma,Willpower=@Willpower,Intelligence=@Intelligence
                WHERE CharacterID = @CharacterID", sqlConn))
            {
                sqlConn.Open();
                cmd.Parameters.Add(new SqlParameter("Gold", unit.Gold));
                cmd.Parameters.Add(new SqlParameter("Experience", unit.Experience));
                cmd.Parameters.Add(new SqlParameter("Level", unit.CharacterLevel));
                cmd.Parameters.Add(new SqlParameter("SkillPoints", unit.SkillPoints));
                
                cmd.Parameters.Add(new SqlParameter("LeftHand", unit.EquipLeftHand == 0 ? DBNull.Value : (object)unit.EquipLeftHand));
                cmd.Parameters.Add(new SqlParameter("RightHand", unit.EquipRightHand == 0 ? DBNull.Value : (object)unit.EquipRightHand));
                cmd.Parameters.Add(new SqlParameter("Head", unit.EquipHead == 0 ? DBNull.Value : (object)unit.EquipHead));
                cmd.Parameters.Add(new SqlParameter("Body", unit.EquipBody == 0 ? DBNull.Value : (object)unit.EquipBody));
                cmd.Parameters.Add(new SqlParameter("Ranged", unit.EquipRanged == 0 ? DBNull.Value : (object)unit.EquipRanged));
                cmd.Parameters.Add(new SqlParameter("FrontRow", unit.FrontRow));

                cmd.Parameters.Add(new SqlParameter("MaxHealth", unit.MaxHealth));
                cmd.Parameters.Add(new SqlParameter("MaxEnergy", unit.MaxEnergy));
                cmd.Parameters.Add(new SqlParameter("Defense", unit.BaseDefense));
                cmd.Parameters.Add(new SqlParameter("Strength", unit.BaseStrength));
                cmd.Parameters.Add(new SqlParameter("Agility", unit.BaseAgility));
                cmd.Parameters.Add(new SqlParameter("Charisma", unit.BaseCharisma));
                cmd.Parameters.Add(new SqlParameter("Willpower", unit.BaseWillpower));
                cmd.Parameters.Add(new SqlParameter("Intelligence", unit.BaseIntelligence));
                
                cmd.Parameters.Add(new SqlParameter("CharacterID", unit.CharacterID));

                cmd.ExecuteNonQuery();
                sqlConn.Close();
            }
        }

        internal void UpdateCharacterPosition(Player unit)
        {
            using (var sqlConn = new SqlConnection(_sqlConnString))
            using (var cmd = new SqlCommand(@"UpdateCharacterPosition", sqlConn))
            {
                sqlConn.Open();
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("CharacterID", unit.CharacterID));
                cmd.Parameters.Add(new SqlParameter("LocationID", unit.CurrentLocation == null ? DBNull.Value : (object)unit.CurrentLocation.LocationID));
                cmd.Parameters.Add(new SqlParameter("SceneID", unit.Scene));
                cmd.Parameters.Add(new SqlParameter("PlayerX", unit.MapPosition.x));
                cmd.Parameters.Add(new SqlParameter("PlayerY", unit.MapPosition.y));

                cmd.ExecuteNonQuery();
                sqlConn.Close();
            }
        }

        internal void UpdateCharacterAbilities(Player unit)
        {
            using (var sqlConn = new SqlConnection(_sqlConnString))
            using (var cmd = new SqlCommand(@"UpdateCharacterAbilities", sqlConn))
            {
                sqlConn.Open();
                cmd.CommandType = CommandType.StoredProcedure;

                var abilities = new DataTable();
                abilities.Columns.Add("AbilityID", typeof(int));
                abilities.Columns.Add("Level", typeof(int));
                
                foreach (var ability in unit.Abilities.Keys)
                {
                    var drItem = abilities.NewRow();

                    drItem["AbilityID"] = ability;
                    drItem["Level"] = unit.Abilities[ability].Level;

                    abilities.Rows.Add(drItem);
                }

                cmd.Parameters.Add(new SqlParameter("CharacterID", unit.CharacterID));
                cmd.Parameters.Add(new SqlParameter("Abilities", abilities));

                cmd.ExecuteNonQuery();
                sqlConn.Close();
            }
        }

        internal void UpdateCharacterInventory(Player unit)
        {
            using (var sqlConn = new SqlConnection(_sqlConnString))
            using (var cmd = new SqlCommand(@"UpdateCharacterInventory", sqlConn))
            {
                sqlConn.Open();
                cmd.CommandType = CommandType.StoredProcedure;

                var inventory = new DataTable();
                inventory.Columns.Add("ItemID", typeof(int));
                inventory.Columns.Add("Quantity", typeof(int));

                var inventoryData = string.Empty;

                foreach (var inventoryItem in unit.GetInventory())
                {
                    var drItem = inventory.NewRow();

                    drItem["ItemID"] = inventoryItem.AbilityID;
                    drItem["Quantity"] = inventoryItem.Quantity;

                    inventory.Rows.Add(drItem);

                    inventoryData += string.Format("{0},{1}|", inventoryItem.AbilityID, inventoryItem.Quantity);
                }
                
                cmd.Parameters.Add(new SqlParameter("CharacterID", unit.CharacterID));
                cmd.Parameters.Add(new SqlParameter("Inventory", inventory));

                cmd.ExecuteNonQuery();
                sqlConn.Close();

                WriteLog(unit.CharacterID, string.Format("Update Inventory - {0}, {1}, {2}", unit.CharacterID, inventoryData, Environment.StackTrace));
            }
        }

        internal void StartQuest(Quest quest, Player unit)
        {
            if (!quest.CheckAvailabilityConditions(unit))
                return;

            using (var sqlConn = new SqlConnection(_sqlConnString))
            {
                sqlConn.Open();
                using (var cmd = new SqlCommand(@"StartQuest", sqlConn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("CharacterID", unit.CharacterID));
                    cmd.Parameters.Add(new SqlParameter("QuestID", quest.QuestID));

                    cmd.ExecuteNonQuery();
                }

                foreach (var killCount in unit.QuestKillCounts.Where(q => q.QuestID == quest.QuestID))
                {
                    using (var cmd = new SqlCommand(@"UpdateQuestKillCount", sqlConn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add(new SqlParameter("QuestID", quest.QuestID));
                        cmd.Parameters.Add(new SqlParameter("CharacterID", unit.CharacterID));
                        cmd.Parameters.Add(new SqlParameter("EnemyID", killCount.EnemyID));
                        cmd.Parameters.Add(new SqlParameter("Quantity", killCount.Quantity));

                        cmd.ExecuteNonQuery();
                    }
                }

                foreach (var talkCount in unit.QuestTalkCounts.Where(q => q.QuestID == quest.QuestID))
                {
                    using (var cmd = new SqlCommand(@"UpdateQuestTalkCount", sqlConn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add(new SqlParameter("QuestID", quest.QuestID));
                        cmd.Parameters.Add(new SqlParameter("CharacterID", unit.CharacterID));
                        cmd.Parameters.Add(new SqlParameter("NPCID", talkCount.NPCID));
                        cmd.Parameters.Add(new SqlParameter("Quantity", talkCount.Quantity));

                        cmd.ExecuteNonQuery();
                    }
                }

                sqlConn.Close();
            }
        }

        internal void UpdateKillCounts(QuestKillCount killCount)
        {
            using (var sqlConn = new SqlConnection(_sqlConnString))
            using (var cmd = new SqlCommand(@"UpdateQuestKillCount", sqlConn))
            {
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("QuestID", killCount.QuestID));
                cmd.Parameters.Add(new SqlParameter("CharacterID", killCount.CharacterID));
                cmd.Parameters.Add(new SqlParameter("EnemyID", killCount.EnemyID));
                cmd.Parameters.Add(new SqlParameter("Quantity", killCount.Quantity));

                sqlConn.Open();
                cmd.ExecuteNonQuery();
                sqlConn.Close();
            }
        }

        internal void UpdateTalkCounts(QuestTalkCount talkCount)
        {
            using (var sqlConn = new SqlConnection(_sqlConnString))
            using (var cmd = new SqlCommand(@"UpdateQuestTalkCount", sqlConn))
            {
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("QuestID", talkCount.QuestID));
                cmd.Parameters.Add(new SqlParameter("CharacterID", talkCount.CharacterID));
                cmd.Parameters.Add(new SqlParameter("NPCID", talkCount.NPCID));
                cmd.Parameters.Add(new SqlParameter("Quantity", talkCount.Quantity));

                sqlConn.Open();
                cmd.ExecuteNonQuery();
                sqlConn.Close();
            }
        }

        internal void CompleteQuest(Quest quest, Player unit)
        {
            if (!quest.CheckRewardConditions(unit))
                return;

            quest.UpdateInventory(unit);
            
            using (var sqlConn = new SqlConnection(_sqlConnString))
            using (var cmd = new SqlCommand(@"CompleteQuest", sqlConn))
            {
                sqlConn.Open();
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("CharacterID", unit.CharacterID));
                cmd.Parameters.Add(new SqlParameter("QuestID", quest.QuestID));

                cmd.ExecuteNonQuery();
                sqlConn.Close();
            }

        }

        internal void PurchaseItem(int itemID, int characterID, int cost, int quantity)
        {
            using (var sqlConn = new SqlConnection(_sqlConnString))
            using (var cmd = new SqlCommand(@"PurchaseItem", sqlConn))
            {
                sqlConn.Open();
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("CharacterID", characterID));
                cmd.Parameters.Add(new SqlParameter("ItemID", itemID));
                cmd.Parameters.Add(new SqlParameter("Cost", cost));
                cmd.Parameters.Add(new SqlParameter("Quantity", quantity));

                cmd.ExecuteNonQuery();
                sqlConn.Close();

                WriteLog(characterID, string.Format("Purchase - {0}, {1}, {2}, {3}, {4}", characterID, itemID, cost, quantity, Environment.StackTrace));
            }
        }

        internal void SellItem(int itemID, int characterID, int cost, int quantity)
        {
            using (var sqlConn = new SqlConnection(_sqlConnString))
            using (var cmd = new SqlCommand(@"SellItem", sqlConn))
            {
                sqlConn.Open();
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("CharacterID", characterID));
                cmd.Parameters.Add(new SqlParameter("ItemID", itemID));
                cmd.Parameters.Add(new SqlParameter("Cost", cost));
                cmd.Parameters.Add(new SqlParameter("Quantity", quantity));

                cmd.ExecuteNonQuery();
                sqlConn.Close();

                WriteLog(characterID, string.Format("Sell - {0}, {1}, {2}, {3}, {4}", characterID, itemID, cost, quantity, Environment.StackTrace));
            }
        }

        internal void WriteLog(int characterID, string logData)
        {
            using (var sqlConn = new SqlConnection(_sqlConnString))
            using (var cmd = new SqlCommand(@"WriteLog", sqlConn))
            {
                sqlConn.Open();
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("CharacterID", characterID));
                cmd.Parameters.Add(new SqlParameter("LogData", logData));

                cmd.ExecuteNonQuery();
                sqlConn.Close();
            }
        }
    }

    public class AuthenticationResult
    {
        public string UserID { get; set; }
        public bool Authenticated { get; set; }
        public int Gold { get; set; }
        public int Experience { get; set; }
        public int Level { get; set; }
        public int CharacterID { get; set; }
    }

    public class CharacterModel
    {
        public int CharacterID { get; set; }
        public int CharacterTypeID { get; set; }
        public string CharacterName { get; set; }
        public int MaxEnergy { get; set; }
        public int MaxHealth { get; set; }
        public int EquipLeftHand { get; set; }
        public int EquipRightHand { get; set; }
        public int EquipHead { get; set; }
        public int EquipBody { get; set; }
        public int EquipRanged { get; set; }

        public double Defense { get; set; }
        public double Strength { get; set; }
        public double Agility { get; set; }
        public double Charisma { get; set; }
        public double Willpower { get; set; }
        public double Intelligence { get; set; }
        public int SkillPoints { get; set; }
        public bool FrontRow { get; set; }

        public int? LocationID { get; set; }
        public int? SceneID { get; set; }
        public double? PlayerX { get; set; }
        public double? PlayerY { get; set; }

        public Dictionary<int, int> Abilities = new Dictionary<int, int>();
        public Dictionary<int, int> Inventory = new Dictionary<int, int>();
        public Dictionary<int, Quest.QuestStatusEnum> PriorQuests = new Dictionary<int, Quest.QuestStatusEnum>();
        public List<QuestKillCount> QuestKillCounts = new List<QuestKillCount>();
        public List<QuestTalkCount> QuestTalkCounts = new List<QuestTalkCount>();
    }

    public class StoreInventory
    {
        public int ItemID { get; set; }
        public int Cost { get; set; }
    }

    public class Store
    {
        public int StoreID { get; set; }
        public int NPCID { get; set; }
    }
}
