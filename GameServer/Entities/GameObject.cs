﻿using GameServer.Networking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace GameServer.Entities
{
    public class GameObject
    {
        public enum AICategory
        {
            None,
            Attacker,
            Healer
        };

        #region Private Member Variables
        private bool _updateNeeded;
        private bool _disposed;
        private AICategory _ai; // Is this character controlled by the AI?
        private ISocketConnection _connection;
        private Guid _gameObjectID; // ID with in our system.
        private string _textureName;
        private string _characterName;
        private bool _ally;
        private int _health;
        private int _maxHealth;
        private int _energy;

        private double _strength;
        private double _agility;
        private double _defense;
        private double _charisma;
        private double _willpower;
        private double _intelligence;        

        private int _maxEnergy;
        private int _runFactor; // Number from 0 to 10 indicating difficulty to egress (10 is impossible)
        private bool _defending; // Use this to trigger when to notify clients defend no longer activated
        private DateTime _defendDuration = DateTime.MinValue;
        private Math.Vector2 _mapPosition;
        private DateTime _coolDown;
        private BattleHandler _currentBattle;
        private Location _currentLocation;
        private int _scene;
        private Dictionary<int, InventoryItem> _inventory; // The items owned by this character
        private List<Buff> _modifiers;

        private InventoryItem _equipLeftHand = null;
        private InventoryItem _equipRightHand = null;
        private InventoryItem _equipHead = null;
        private InventoryItem _equipBody = null;
        private InventoryItem _equipRanged = null;
        #endregion

        #region Public Properties
        public AICategory AI
        {
            get
            {
                return _ai;
            }
        }

        public ISocketConnection Connection
        {
            get
            {
                return _connection;
            }
        }

        public Guid GameObjectID
        {
            get
            {
                return _gameObjectID;
            }
        }

        public bool Disposed
        {
            get
            {
                return _disposed;
            }
        }

        public string TextureName
        {
            get
            {
                return _textureName;
            }
        }

        public string CharacterName
        {
            get
            {
                return _characterName;
            }
        }

        public bool Ally
        {
            get
            {
                return _ally;
            }
        }

        public int Health
        {
            get
            {
                return _health;
            }
        }

        public int MaxHealth
        {
            get
            {
                return _maxHealth;
            }
        }

        public int Energy
        {
            get
            {
                return _energy;
            }
        }

        public int MaxEnergy
        {
            get
            {
                return _maxEnergy;
            }
        }

        public double BaseStrength
        {
            get
            {
                return _strength;
            }
        }

        public int Strength
        {
            get
            {
                return (int)_strength + _modifiers.Sum(m => m.StrengthOffset);
            }
        }

        public double BaseDefense
        {
            get
            {
                return _defense;
            }
        }

        public int Defense
        {
            get
            {
                return (int)_defense + _modifiers.Sum(m => m.DefenseOffset);
            }
        }

        public double BaseAgility
        {
            get
            {
                return _agility;
            }
        }

        public int Agility
        {
            get
            {
                return (int)_agility + _modifiers.Sum(m => m.AgilityOffset);
            }
        }

        public double BaseCharisma
        {
            get
            {
                return _charisma;
            }
        }

        public int Charisma
        {
            get
            {
                return (int)_charisma + _modifiers.Sum(m => m.CharismaOffset);
            }
        }

        public double BaseWillpower
        {
            get
            {
                return _willpower;
            }
        }

        public int Willpower
        {
            get
            {
                return (int)_willpower + _modifiers.Sum(m => m.WillpowerOffset);
            }
        }

        public double BaseIntelligence
        {
            get
            {
                return _intelligence;
            }
        }

        public int Intelligence
        {
            get
            {
                return (int)_intelligence + _modifiers.Sum(m => m.IntelligenceOffset);
            }
        }

        public Math.Vector2 MapPosition
        {
            get
            {
                return _mapPosition;
            }
        }

        public DateTime CoolDown
        {
            get
            {
                return _coolDown;
            }
        }

        public BattleHandler CurrentBattle
        {
            get
            {
                return _currentBattle;
            }
        }

        public Location CurrentLocation
        {
            get
            {
                return _currentLocation;
            }
        }

        public int Scene
        {
            get
            {
                return _scene;
            }
        }

        public bool FrontRow { get; set; }
        
        public string BuffString
        {
            get
            {
                string result = string.Empty;

                int defense = 0;
                int strength = 0;
                int agility = 0;
                int charisma = 0;
                int willpower = 0;
                int intelligence = 0;

                foreach(var modifier in _modifiers)
                {
                    defense += modifier.DefenseOffset;
                    strength += modifier.StrengthOffset;
                    agility += modifier.AgilityOffset;
                    charisma += modifier.CharismaOffset;
                    willpower += modifier.WillpowerOffset;
                    intelligence += modifier.IntelligenceOffset;
                }

                if (defense != 0)
                    result += "D~" + defense + "^";
                if (strength != 0)
                    result += "S~" + strength + "^";
                if (agility != 0)
                    result += "A~" + agility + "^";
                if (charisma != 0)
                    result += "C~" + charisma + "^";
                if (willpower != 0)
                    result += "W~" + willpower + "^";
                if (intelligence != 0)
                    result += "I~" + intelligence + "^";

                return result;
            }
        }

        public bool UpdateNeeded
        {
            get
            {
                if (_defending && _defendDuration < DateTime.UtcNow)
                {
                    _defending = false;
                    _updateNeeded = true;
                }

                return _updateNeeded;
            }
        }

        public bool UpdateInventoryNeeded { get; private set; }

        public Dictionary<int, Ability> Abilities { get; private set; }

        public List<InventoryItem> Equipment
        {
            get
            {
                List<InventoryItem> result = new List<InventoryItem>();
                if (_equipBody != null)
                    result.Add(_equipBody);
                if (_equipHead != null)
                    result.Add(_equipHead);
                if (_equipLeftHand != null)
                    result.Add(_equipLeftHand);
                if (_equipRightHand != null)
                    result.Add(_equipRightHand);
                if (_equipRanged != null)
                    result.Add(_equipRanged);

                return result;
            }
        }

        public bool Dead { get; set; }
        
        #endregion

        /// <summary>
        /// Construct new game object
        /// </summary>
        /// <param name="connection">Network connection object</param>
        public GameObject(AICategory ai, ISocketConnection connection, string textureName, CharacterModel character, bool ally,
            Dictionary<int, Ability> abilities, Dictionary<int, InventoryItem> inventory)
        {
            _ai = ai;
            _gameObjectID = Guid.NewGuid();

            _modifiers = new List<Buff>();

            _connection = connection;
            _textureName = textureName;
            
            _ally = ally;
            Abilities = abilities;
            _inventory = inventory;

            _characterName = character.CharacterName;
            _health = character.MaxHealth;
            _maxHealth = character.MaxHealth;
            _energy = character.MaxEnergy;
            _maxEnergy = character.MaxEnergy;

            _defense = character.Defense;
            _strength = character.Strength;
            _agility = character.Agility;
            _charisma = character.Charisma;
            _willpower = character.Willpower;
            _intelligence = character.Intelligence;
            FrontRow = character.FrontRow;

            if (character.SceneID.HasValue)
                _scene = character.SceneID.Value;
            if (character.PlayerX.HasValue && character.PlayerY.HasValue)
                _mapPosition = new Math.Vector2((float)character.PlayerX.Value, (float)character.PlayerY.Value);
            else
                _mapPosition = new Math.Vector2(-10.5f, 0.1f);
            
            _coolDown = DateTime.MinValue;
            _currentBattle = null;
            _currentLocation = null;

        }

        /// <summary>
        /// Construct a game object from a known template
        /// </summary>
        /// <param name="template"></param>
        public GameObject(GameObject template)
        {
            _gameObjectID = Guid.NewGuid();

            _modifiers = new List<Buff>();

            _connection = null;
            _ai = template.AI;
            _textureName = template.TextureName;
            _characterName = template.CharacterName;
            _ally = template.Ally;
            
            _health = template.Health;
            _maxHealth = template.MaxHealth;
            _energy = template.Energy;
            _maxEnergy = template.MaxEnergy;

            _strength = template.Strength;
            _defense = template.Defense;
            _agility = template.Agility;
            _charisma = template.Charisma;
            _willpower = template.Willpower;
            _intelligence = template.Intelligence;

            Abilities = template.Abilities;
            FrontRow = true;

            _inventory = new Dictionary<int, InventoryItem>();
            foreach (var inventory in template._inventory.Values)
            {
                _inventory[inventory.AbilityID] = LevelHandler.InventoryTemplate(inventory.AbilityID, inventory.Quantity);
            }
        }

        public int EquipLeftHand
        {
            get
            {
                return _equipLeftHand == null ? 0 : _equipLeftHand.AbilityID;
            }
            set
            {
                if (value == 0 || !_inventory.ContainsKey(value))
                {
                    _equipLeftHand = null;
                }
                else
                {
                    InventoryItem item = _inventory[value];

                    if (item.ItemType == InventoryItem.ItemTypeEnum.TwoHand)
                    {
                        _equipLeftHand = item;
                        _equipRightHand = item;
                    }
                    else if (_equipRightHand != null && value == _equipRightHand.AbilityID)
                    {
                        // Now we need two
                        if (item.Quantity > 1)
                            _equipLeftHand = item;
                        else
                            _equipLeftHand = null;
                    }
                    else
                    {
                        if (item.Quantity > 0)
                            _equipLeftHand = item;
                        else
                            _equipLeftHand = null;
                    }
                }
            }
        }

        public int EquipRightHand
        {
            get
            {
                return _equipRightHand == null ? 0 : _equipRightHand.AbilityID;
            }
            set
            {
                if (value == 0 || !_inventory.ContainsKey(value))
                {
                    _equipRightHand = null;
                }
                else
                {
                    InventoryItem item = _inventory[value];

                    if (item.ItemType == InventoryItem.ItemTypeEnum.TwoHand)
                    {
                        _equipLeftHand = item;
                        _equipRightHand = item;
                    }
                    else if (_equipLeftHand != null && value == _equipLeftHand.AbilityID)
                    {
                        // Now we need two
                        if (item.Quantity > 1)
                            _equipRightHand = item;
                        else
                            _equipRightHand = null;
                    }
                    else
                    {
                        if (item.Quantity > 0)
                            _equipRightHand = item;
                        else
                            _equipRightHand = null;
                    }
                }
            }
        }

        public int EquipHead
        {
            get
            {
                return _equipHead == null ? 0 : _equipHead.AbilityID;
            }
            set
            {
                if (value == 0)
                    _equipHead = null;
                if (_inventory.ContainsKey(value) && _inventory[value].Quantity > 0)
                    _equipHead = _inventory[value];
                else
                    _equipHead = null;
            }
        }

        public int EquipBody
        {
            get
            {
                return _equipBody == null ? 0 : _equipBody.AbilityID;
            }
            set
            {
                if (value == 0)
                    _equipBody = null;
                if (_inventory.ContainsKey(value) && _inventory[value].Quantity > 0)
                    _equipBody = _inventory[value];
                else
                    _equipBody = null;
            }
        }

        public int EquipRanged
        {
            get
            {
                return _equipRanged == null ? 0 : _equipRanged.AbilityID;
            }
            set
            {
                if (value == 0)
                    _equipRanged = null;
                if (_inventory.ContainsKey(value) && _inventory[value].Quantity > 0)
                    _equipRanged = _inventory[value];
                else
                    _equipRanged = null;
            }
        }

        public void GainLevel()
        {
            _maxHealth += (int)((double)_maxHealth * .15);
            _maxEnergy += (int)((double)_maxEnergy * .15);
            
            _defense += BaseDefense * .15;
            _strength += BaseStrength * .15;
            _agility += BaseAgility * .15;
            _charisma += BaseCharisma * .15;
            _willpower += BaseWillpower * .15;
            _intelligence += BaseIntelligence * .15;
        }

        #region Methods

        public static Player InitializePlayer(ISocketConnection connection, CharacterModel character, string userID)
        {
            Dictionary<int, Ability> abilities = new Dictionary<int, Ability>();
            Dictionary<int, InventoryItem> inventory = new Dictionary<int, InventoryItem>();

            foreach (KeyValuePair<int, int> inventoryNode in character.Inventory)
            {
                inventory[inventoryNode.Key] = LevelHandler.InventoryTemplate(inventoryNode.Key, inventoryNode.Value);
            }

            foreach (var abilityID in character.Abilities.Keys)
            {
                abilities[abilityID] = LevelHandler.AbilityTemplate(abilityID, character.Abilities[abilityID]);
            }

            string textureName = string.Empty;
            switch (character.CharacterTypeID)
            {
                case 1:
                    textureName = "woodsman";
                    break;
                case 2:
                    textureName = "merchant";
                    break;
                default:
                    textureName = "Unknown";
                    break;
            }

            Player result = new Player(AICategory.None, connection, textureName, character, true, abilities, inventory);

            result.UserID = userID;
            result.CharacterID = character.CharacterID;
            result.CharacterTypeID = character.CharacterTypeID;
            result.EquipLeftHand = character.EquipLeftHand;
            result.EquipRightHand = character.EquipRightHand;
            result.EquipHead = character.EquipHead;
            result.EquipBody = character.EquipBody;
            result.EquipRanged = character.EquipRanged;

            foreach(var ability in abilities.Values.Where(ab => !ab.Active))
            {
                ability.CalculateBuff(result, result).ForEach(b => result.ResolveModifier(b));
            }

            return result;
        }

        public void MarkDisposed()
        {
            _updateNeeded = true;
            _disposed = true;
        }

        public void MarkUpdated()
        {
            _updateNeeded = false;
        }

        public void MarkInventoryUpdated()
        {
            UpdateInventoryNeeded = false;
        }

        public void CheckBuffs()
        {
            for (int index = _modifiers.Count; index > 0; index--)
            {
                if (_modifiers[index - 1].Expiry < DateTime.UtcNow)
                {
                    _modifiers.RemoveAt(index - 1);
                    _updateNeeded = true;
                }
            }
        }

        public void UseAttack(GameObject target)
        {
            if (_currentBattle == null)
                return;
            
            _currentBattle.InitiateAbility(-1, target, this);
            Ability.CastAttack(target, this, null);
        }

        public void UseAbility(int abilityId, GameObject target)
        {
            if (Abilities.ContainsKey(abilityId))
            {
                Ability ability = Abilities[abilityId];

                if (ability != null)
                {
                    if (_currentBattle != null)
                        _currentBattle.InitiateAbility(abilityId, target, this);
                    ability.CastAbility(target, this);
                }
            }
        }

        public int ReduceInventory(int inventoryItemId)
        {
            if (_inventory.ContainsKey(inventoryItemId))
            {
                InventoryItem item = _inventory[inventoryItemId];

                if (item != null)
                {
                    item.Quantity--;
                    UpdateInventoryNeeded = true;
                    
                    if (item.Quantity <= 0)
                    {
                        _inventory.Remove(inventoryItemId);
                    }

                    return item.Quantity;
                }
            }

            return 0;
        }

        public void UseInventory(int inventoryItemId, GameObject target)
        {
            if (_inventory.ContainsKey(inventoryItemId))
            {
                InventoryItem item = _inventory[inventoryItemId];

                if (item != null)
                {
                    if (target != null)
                        item.CastAbility(target, this);

                    if (item.ItemType == InventoryItem.ItemTypeEnum.Consumable)
                    {
                        ReduceInventory(inventoryItemId);
                    }
                }
            }
        }
        
        public int AddInventory(InventoryItem inventoryItem)
        {
            InventoryItem item = null;
            if (!_inventory.ContainsKey(inventoryItem.AbilityID))
            {
                item = inventoryItem.Clone();
                _inventory[inventoryItem.AbilityID] = item;
            }
            else
            {
                item = _inventory[inventoryItem.AbilityID];
            }

             item.Quantity++;
             UpdateInventoryNeeded = true;

             return item.Quantity;
        }

        public List<InventoryItem> GetInventory()
        {
            return _inventory.Values.ToList();
        }

        internal int InventoryItemQty(int FetchItemID)
        {
            if (!_inventory.ContainsKey(FetchItemID))
                return 0;
            else
                return _inventory[FetchItemID].Quantity;
        }

        internal int InventoryItemCost(int FetchItemID)
        {
            if (!_inventory.ContainsKey(FetchItemID))
                return 0;
            else
                return (int)(_inventory[FetchItemID].Cost * 0.5);
        }

        public void TakeDamage(int damage)
        {
            _health -= damage;

            if (_health <= 0)
            {
                _disposed = true;
            }

            if (_health > _maxHealth)
                _health = _maxHealth;

            if (_currentBattle != null)
            {
                // Show the "-10" on the screen
                _currentBattle.UpdateHealth(this, damage);
            }
            _updateNeeded = true;
        }

        public void ResolveEffect(Ability ability, GameObject caster)
        {
            List<Buff> buffs = ability.CalculateBuff(caster, caster);

            if (ability.DamageType == Ability.DamageTypeEnum.Physical)
            {
                Ability.CastAttack(this, caster, buffs);

                if (ability.StrikeTwice)
                {
                    if (ability.InitialHitChanceIncrease > 0)
                    {
                        buffs.Add(new Buff() { AgilityOffset = caster.Agility * ability.InitialHitChanceIncrease });
                    }
                
                    Ability.CastAttack(this, caster, buffs);
                }
            }
            else
            {
                if (ability.Strength > 0)
                {
                    int damage = ability.Strength + caster.Intelligence;


                    // TODO: Elemental spell ability?
                    float elementalModifier = 1.0f;
                    damage = (int)((float)damage * elementalModifier);

                    damage -= this.Willpower;


                    TakeDamage(damage);
                }
            }

            if (buffs != null)
                buffs.ForEach(b => ResolveModifier(b));
        }

        public void ResolveCasting(Ability ability)
        {
            UpdateCoolDown(6);
            UpdateEnergy(-ability.Cost);
        }

        private void ResolveModifier(Buff modifier)
        {
            if (modifier.DefaultDuration > 0)
                _modifiers.Add(modifier.Initialize());
            else
            {
                modifier.ApplyInstants(this);
            }
        }

        public void UpdateCoolDown(int coolDown)
        {
            if (_coolDown < DateTime.UtcNow)
                _coolDown = DateTime.UtcNow.AddSeconds(coolDown);
            else
                _coolDown = _coolDown.AddSeconds(coolDown);
            _updateNeeded = true;
        }

        public bool CheckEnergy(int energyCost)
        {
            Buff costPercent = _modifiers.FirstOrDefault(m => m.AbilityCostPercent != 0);
            if (costPercent != null)
            {
                energyCost = (int)((double)energyCost * costPercent.AbilityCostPercent);
            }

            if (energyCost < 0 && _energy + energyCost < 0)
                return false; // Not enough energy
            else
                return true;
        }

        public void UpdateEnergy(int energyCost)
        {
            _energy += energyCost;
            _updateNeeded = true;
        }

        public void UpdatePosition(Math.Vector2 position, int scene)
        {
            _mapPosition = position;
            _scene = scene;
        }

        public void EnterBattle(BattleHandler battleHandler)
        {
            _currentBattle = battleHandler;
        }

        public void LeaveBattle()
        {
            _currentBattle = null;
        }

        public void EnterLocation(Location location)
        {
            _currentLocation = location;
            _scene = 0;
        }

        public void LeaveLocation()
        {
            _currentLocation = null;
        }

        #endregion

        public void Defend()
        {
            _defending = true;
            _coolDown = DateTime.UtcNow.AddSeconds(10);
            _defendDuration = _coolDown;
            _updateNeeded = true;
        }

        public void SwitchRow()
        {
            FrontRow = !FrontRow;
            _coolDown = DateTime.UtcNow.AddSeconds(10);
            _defendDuration = _coolDown;
            _updateNeeded = true;
        }

        internal void Resurect()
        {
            _disposed = false;
            Dead = false;
            _health = MaxHealth;
            _energy = (int)((double)MaxEnergy * 0.80);
        }

        public bool AttemptEscape()
        {
            // Weight escape success on agility of enemies against character agility
            int runFactor = 0;

            if (CurrentBattle != null && CurrentBattle.GameObjects.Values.Any(go => go.Ally != _ally))
            {
                runFactor = CurrentBattle.GameObjects.Values.Where(go => go.Ally != _ally).Max(go => go._runFactor);
            }

            UpdateCoolDown(10); // Run costs 10 seconds

            Random rnd = new Random();
            int runSuccess = rnd.Next(0, 9);

            return runSuccess > runFactor;
        }

        internal void MakeAction(BattleHandler battle, Random rnd)
        {
            if (_coolDown > DateTime.UtcNow)
                return;

            // Random cooldown extension to create effect of character "thinking"
            double cooldownExtend = (double)rnd.Next(5, 20) / 10;
                
            if (_ai == AICategory.Healer)
            {
                var healTarget = battle.GameObjects.Values.Where(go => go.Ally == _ally && go.Health < go.MaxHealth)
                    .OrderByDescending(go => go.MaxHealth - go.Health).FirstOrDefault();

                if (healTarget != null)
                {
                    // heal the heal target
                    Ability healAbility = Abilities.Values.FirstOrDefault(a => a.Strength < 0);

                    if (healAbility != null && CheckEnergy(healAbility.Cost))
                    {
                        UseAbility(healAbility.AbilityID, healTarget);

                        _coolDown = _coolDown.AddMilliseconds(cooldownExtend * 1000);
                        return;
                    }
                }
            }
            
            // Assume brawler as base case if not ability was used
            List<GameObject> targets = battle.GameObjects.Values.Where(go => go.Ally != _ally).ToList();
            if (targets.Count > 0)
            {
                int targetIndex = rnd.Next(targets.Count);
                var target = targets[targetIndex];

                // attack the target
                UseAttack(target);
            }
            else
            {
                // No action possible - Defend
                Defend();
            }
            _coolDown = _coolDown.AddMilliseconds(cooldownExtend * 1000);
        }

        internal void UpdateCosts(int abilityCostPercent, int itemReusePercent, int defaultDuration)
        {
            DateTime expiry;
            if (defaultDuration == 0)
                expiry = DateTime.MaxValue;
            else
                expiry = DateTime.UtcNow.AddSeconds(defaultDuration);

            _modifiers.Add(new Buff()
                {
                    AbilityCostPercent = ((double)abilityCostPercent * 0.01),
                    ItemReusePercent = ((double)itemReusePercent * 0.01),
                    Expiry = expiry
                });
        }
    }
}
