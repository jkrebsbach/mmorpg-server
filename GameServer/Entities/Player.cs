﻿using GameServer.Networking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameServer.Entities
{
    public class Player : GameObject
    {
        public string UserID { get; set; }
        public int CharacterID { get; set; }
        public int CharacterLevel { get; set; }
        public int CharacterTypeID { get; set; }
        public int SkillPoints { get; set; }

        private long _gold;
        private long _experience;

        private DateTime _respawnTimer;
        private DateTime _positionUpdateTimer;

        public Dictionary<int, Quest.QuestStatusEnum> PriorQuests { get; set; }
        public List<QuestKillCount> QuestKillCounts { get; set; }
        public List<QuestTalkCount> QuestTalkCounts { get; set; }

        public Player(AICategory ai, ISocketConnection connection, string textureName, CharacterModel character, 
            bool ally, Dictionary<int, Ability> abilities, Dictionary<int, InventoryItem> inventory) :
            base (ai, connection, textureName, character, ally, abilities, inventory)
        {
        }

        public int Gold
        {
            get
            {
                return (int)_gold;
            }
        }

        public int Experience
        {
            get
            {
                return (int)_experience;
            }
        }

        public void AddGold(int gold)
        {
            if (_gold + gold > Int32.MaxValue)
                _gold = Int32.MaxValue;
            else
                _gold += gold;
        }

        public bool AddExperience(int experience)
        {
            if (_experience + experience > Int32.MaxValue)
                _experience = Int32.MaxValue;
            else
                _experience += experience;


            return CheckLevel();
        }

        private bool CheckLevel()
        {
            CharacterType charType = LevelHandler.CharacterTypeTemplate(CharacterTypeID);

            if (_experience >= charType.ExperienceLevels[CharacterLevel])
            {
                CharacterLevel++;
                SkillPoints++;
                GainLevel();
                return true;
            }

            return false;
        }

        internal BattleRewardPacket AllocateBattleRewards(int baseExperience, int baseGold, List<EnemyDrop> baseLoot, float battleRatio, Random random)
        {
            int experience = (int)((float)baseExperience * battleRatio);
            int gold = (int)((float)baseGold * battleRatio);

            AddGold(gold);
            bool levelUp = AddExperience(experience);

            List<EnemyDrop> obtainedLoot = new List<EnemyDrop>();
            foreach(var loot in baseLoot)
            {
                if (random.Next(1, 101) <= loot.DropFrequency)
                {
                    AddInventory(LevelHandler.InventoryTemplate(loot.InventoryItemID, 1));
                    obtainedLoot.Add(loot);
                }
            }


            return new BattleRewardPacket()
            {
                Player = this,
                Experience = experience,
                Gold = gold,
                Items = obtainedLoot,
                LevelUp = levelUp
            };
        }

        internal void CheckQuestKillReward(int enemyID, SkyFish database)
        {
            foreach(var killCount in QuestKillCounts.Where(q => q.EnemyID == enemyID))
            {
                killCount.Quantity++;

                database.UpdateKillCounts(killCount);
            }
        }

        internal int CheckQuestTalkReward(int npcID, SkyFish database)
        {
            int result = 0;

            foreach (var talkCount in QuestTalkCounts.Where(q => q.NPCID == npcID))
            {
                talkCount.Quantity++;
                database.UpdateTalkCounts(talkCount);
                result = talkCount.QuestID;
                break;
            }

            return result;
        }

        internal void InitiateQuest(Quest startQuest)
        {
            PriorQuests[startQuest.QuestID] = Quest.QuestStatusEnum.Started;

            foreach(var req in startQuest.QuestRequirements)
            {
                if (req.HuntEnemyQuantity > 0)
                {
                    QuestKillCounts.Add(new QuestKillCount()
                        {
                            QuestID = startQuest.QuestID,
                            EnemyID = req.HuntEnemyTypeID,
                            Quantity = 0,
                            CharacterID = this.CharacterID
                        });
                }
                if (req.TalkNPCID > 0)
                {
                    QuestTalkCounts.Add(new QuestTalkCount()
                    {
                        QuestID = startQuest.QuestID,
                        NPCID = req.TalkNPCID,
                        Quantity = 0,
                        CharacterID = this.CharacterID
                    });
                }
            }
        }

        internal void CompleteQuest(Quest completeQuest)
        {
            PriorQuests[completeQuest.QuestID] = Quest.QuestStatusEnum.Completed;

            QuestKillCounts.RemoveAll(q => q.QuestID == completeQuest.QuestID);
            QuestTalkCounts.RemoveAll(q => q.QuestID == completeQuest.QuestID);
        }

        internal bool LearnAbility(int abilityID)
        {
            if (SkillPoints > 0)
            {
                int level = 0;
                if (Abilities.ContainsKey(abilityID))
                {
                    level = Abilities[abilityID].Level;
                }
                level++;

                Ability newAbility = LevelHandler.AbilityTemplate(abilityID, level);

                if (newAbility.CharacterTypeID.Contains(CharacterTypeID))
                {
                    SkillPoints--;
                    Abilities[abilityID] = LevelHandler.AbilityTemplate(abilityID, level);

                    return true;
                }
            }

            return false;
        }

        internal void Respawn()
        {
            _respawnTimer = DateTime.UtcNow.AddSeconds(10);
            Dead = true;
            UpdatePosition(new Math.Vector2(0, 0), 0);
        }

        internal bool CheckPositionUpdateTimer()
        {
            bool result = (DateTime.UtcNow > _positionUpdateTimer);

            if (result)
                _positionUpdateTimer = DateTime.UtcNow.AddSeconds(30);

            return result;
        }

        internal bool CheckRespawnTimer()
        {
            return DateTime.UtcNow > _respawnTimer;
        }
    }

    public class BattleRewardPacket
    {
        public Player Player;
        public int Experience;
        public int Gold;
        public List<EnemyDrop> Items;
        public bool LevelUp;
    }
}
