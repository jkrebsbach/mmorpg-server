﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameServer.Entities
{
    public class BattleHandler
    {
        private readonly Guid _battleID;
        private readonly Dictionary<Guid, GameObject> _gameObjects;
        private readonly GameServer _gameServer;
        private readonly int _locationID;
        private readonly int _scene; // The scene within the location (if applicable)
        private readonly string _resource;
        private Math.Vector2 _mapPosition;
        private readonly Math.Vector2 _originalPosition;
        private Math.Vector2 _overworldWander;

        private readonly int _baseExperience;
        private readonly int _baseGold;
        private readonly List<EnemyDrop> _baseLoot;
        private DateTime _wanderLastChange;
        private const float WanderSpeed = 1.0f;

        public Dictionary<Guid, GameObject> GameObjects
        {
            get
            {
                return _gameObjects;
            }
        }

        public Guid BattleID
        {
            get
            {
                return _battleID;
            }
        }

        public int LocationID
        {
            get
            {
                return _locationID;
            }
        }

        public int Scene
        {
            get
            {
                return _scene;
            }
        }

        public string Resource
        {
            get
            {
                return _resource;
            }
        }

        public Math.Vector2 OverworldPosition
        {
            get
            {
                return _mapPosition;
            }
        }

        public Math.Vector2 OverworldWander
        {
            get
            {
                return _overworldWander;
            }
        }

        public BattleHandler(List<GameObject> gameObjects, GameServer gameServer, int locationID, int scene, Math.Vector2 mapPosition)
        {
            _battleID = Guid.NewGuid();
            _locationID = locationID;
            _scene = scene;
            _gameServer = gameServer;
            _mapPosition = mapPosition;
            _originalPosition = mapPosition;
            _overworldWander = new Math.Vector2(0f, 0f);
            _gameObjects = new Dictionary<Guid,GameObject>();

            _baseExperience = 0;
            _baseGold = 0;
            _baseLoot = new List<EnemyDrop>();

            _wanderLastChange = DateTime.UtcNow;

            foreach (GameObject combatant in gameObjects)
            {
                Enemy enemy = combatant as Enemy;

                if (enemy != null)
                {
                    _resource = enemy.TextureName;

                    _baseExperience += enemy.Experience;
                    _baseGold += enemy.Gold;
                    _baseLoot = _baseLoot.Concat(enemy.Drops).ToList();
                }

                JoinBattle(combatant);
            }
        }

        public void InitiateAbility(int abilityID, GameObject target, GameObject caster)
        {
            _gameServer.SendInitiateAbility(this, abilityID, target, caster);
        }

        public void UpdateHealth(GameObject target, int damage)
        {
            _gameServer.SendUpdateHealth(this, target, damage);
        }

        public void JoinBattle(GameObject gameObject)
        {
            _gameObjects[gameObject.GameObjectID] = gameObject;

            gameObject.EnterBattle(this);
        }

        public void LeaveBattle(GameObject gameObject)
        {
            if (_gameObjects.ContainsKey(gameObject.GameObjectID))
                _gameObjects.Remove(gameObject.GameObjectID);

            gameObject.LeaveBattle();
        }

        public GameObject FindTarget(Guid guid)
        {
            if (_gameObjects.ContainsKey(guid))
                return _gameObjects[guid];

            return null;
        }

        internal void EnemyActions(Random rnd)
        {
            foreach(var gameObject in GameObjects.Values.Where(go => go.AI != GameObject.AICategory.None))
            {
                gameObject.MakeAction(this, rnd);
            }
        }

        /// <summary>
        /// 10% chance battle will change direction
        /// </summary>
        internal void UpdateBattleWanderDirection(Random rnd, List<GameObject> gameObjects)
        {
            var now = DateTime.UtcNow;

            // Battles only wander when empty
            if (GameObjects.Values.Any(go => go.Connection != null))
            {
                _overworldWander = new Math.Vector2(0, 0);
                _wanderLastChange = now;
                return;
            }

            float timeDelta = now.Subtract(_wanderLastChange).Ticks / TimeSpan.TicksPerSecond;
            var updateDistance = timeDelta * WanderSpeed;
            _mapPosition = _mapPosition.Add(_overworldWander.Times(updateDistance));

            _wanderLastChange = now;

            // Update wander direction
            if (!_originalPosition.Overlap(_mapPosition, 3))
            {
                // If we are outside of safety radius, go back home
                _overworldWander = _originalPosition.Subtract(_mapPosition).Normalize();
            }
            else
            {
                // Else - 40 % chance battle will change direction
                int random = rnd.Next(100);

                if (random > 60)
                {
                    var nearby = gameObjects.Where(go => go.Connection != null && ((_locationID == 0 && go.CurrentLocation == null) ||
                        (go.CurrentLocation != null && go.CurrentLocation.LocationID == _locationID && go.Scene == _scene)));

                    var nearest = gameObjects.OrderByDescending(go => go.MapPosition.Subtract(_mapPosition).Magnitude).FirstOrDefault();

                    if (nearest != null)
                    {
                        _overworldWander = nearest.MapPosition.Subtract(_mapPosition).Normalize();
                    }
                    else
                    {
                        float randomX = rnd.Next(20) - 10;
                        float randomY = rnd.Next(20) - 10;
                        _overworldWander = new Math.Vector2(randomX / 10.0f, randomY / 10.0f);
                    }
                }
            }



        }

        internal List<BattleRewardPacket> FinishBattle(Random random)
        {
            var packets = new List<BattleRewardPacket>();

            var battleRatio = 1f / (float)GameObjects.Values.Count(go => go.Ally);
            foreach(var gameObject in GameObjects.Values.Where(go => go.Ally))
            {
                var player = gameObject as Player;
                if (player != null)
                {
                    packets.Add(player.AllocateBattleRewards(_baseExperience, _baseGold, _baseLoot, battleRatio, random));
                }
            }

            return packets;
        }
    }
}
