﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameServer.Entities
{
    public class InventoryItem : Ability
    {
        public enum ItemTypeEnum
        {
            Consumable = 1,
            Hand = 2,
            TwoHand = 3,
            Head = 4,
            Body = 5,
            Durable = 6,
            Ranged = 7
        };

        public int Quantity;
        public ItemTypeEnum ItemType;
        public int Weight;

        public InventoryItem(int quantity, int abilityID, string abilityName, int strength, int defense, int cost, ItemTypeEnum itemType, int weight, List<int> characterTypes, ElementalEnum element, List<Buff> buffs)
            : base(abilityID, abilityName, 1, strength, defense, cost, true, characterTypes, element, buffs)
        {
            Quantity = quantity;
            Weight = weight;
            ItemType = itemType;
        }

        public InventoryItem Clone()
        {
            InventoryItem result = new InventoryItem(0, AbilityID, AbilityName, Strength, Defense, Cost, ItemType, Weight, CharacterTypeID, Element, Modifiers);

            return result;
        }
    }
}
