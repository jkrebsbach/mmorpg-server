﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameServer.Entities
{
    public class Buff
    {
        public int ItemID;
        public int DefenseOffset;
        public int StrengthOffset;
        public int AgilityOffset;
        public int CharismaOffset;
        public int IntelligenceOffset;
        public int WillpowerOffset;
        public int HealthOffset;
        public bool CasterEffect;
        public double AbilityCostPercent;
        public double ItemReusePercent;
        public bool BackRow;

        public int DefaultDuration;
        public DateTime Expiry;

        internal Buff Initialize()
        {
            Buff buff = new Buff()
            {
                DefenseOffset = this.DefenseOffset,
                StrengthOffset = this.StrengthOffset,
                AgilityOffset = this.AgilityOffset,
                CharismaOffset = this.CharismaOffset,
                WillpowerOffset = this.WillpowerOffset,
                HealthOffset = this.HealthOffset,
                BackRow = this.BackRow,

                Expiry = DateTime.UtcNow.AddSeconds(this.DefaultDuration)
            };

            return buff;
        }

        internal void ApplyInstants(GameObject gameObject)
        {
            if (HealthOffset != 0)
                gameObject.TakeDamage(HealthOffset);
        }
    }
}
