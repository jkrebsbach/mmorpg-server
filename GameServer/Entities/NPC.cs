﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameServer.Entities
{
    public class NPC
    {
        public int NPCID { get; set; }
        public int LocationID { get; set; }
        public int Scene { get; set; }
        public Math.Vector2 MapPosition { get; set; }

        public NPC(int npcID, int location, int scene, Math.Vector2 mapPosition)
        {
            NPCID = npcID;
            LocationID = location;
            Scene = scene;
            MapPosition = mapPosition;
        }
    }
}
