﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameServer.Entities
{
    public class Quest
    {
        public enum QuestStatusEnum
        {
            NotStarted = 0,
            Started = 1,
            Completed = 2
        }

        public int QuestID { get; set; }
        public string Description { get; set; }
        public QuestStatusEnum Status { get; set; }
        public bool RewardConditionsMet { get; set; }
        public bool AvailabilityConditionsMet { get; set; }

        internal bool CheckRewardConditions(Player unit)
        {
            if (Status == QuestStatusEnum.Started)
            {
                RewardConditionsMet = true;

                foreach (var condition in QuestRequirements)
                {
                    if (!condition.CheckRequirement(unit, QuestID))
                    {
                        RewardConditionsMet = false;
                        break;
                    }
                }
            }
            else
                RewardConditionsMet = false;

            return RewardConditionsMet;
        }

        internal void UpdateInventory(Player unit)
        {
            foreach (var condition in QuestRequirements)
            {
                condition.UpdateInventory(unit);
            }
        }

        internal bool CheckAvailabilityConditions(Player unit)
        {
            if (Status == QuestStatusEnum.NotStarted)
            {
                AvailabilityConditionsMet = true;

                if (QuestCondition != null)
                {
                    AvailabilityConditionsMet = (QuestCondition.CharacterType == 0 || unit.CharacterTypeID == QuestCondition.CharacterType);

                    if (unit.CharacterLevel < QuestCondition.CharacterMinimumLevel)
                        AvailabilityConditionsMet = false;

                    if (AvailabilityConditionsMet)
                    {
                        foreach (var condition in QuestCondition.Quests)
                        {
                            if (!unit.PriorQuests.ContainsKey(condition) || unit.PriorQuests[condition] != QuestStatusEnum.Completed)
                            {
                                AvailabilityConditionsMet = false;
                                break;
                            }
                        }
                    }
                }
            }
            else
                AvailabilityConditionsMet = false;

            return AvailabilityConditionsMet;
        }

        internal void CheckConditions(Player unit)
        {
            CheckRewardConditions(unit);
            CheckAvailabilityConditions(unit);            
        }

        public List<QuestReward> QuestRewards = new List<QuestReward>();
        public List<QuestRequirement> QuestRequirements = new List<QuestRequirement>();
        public QuestCondition QuestCondition = null;

        internal void ApplyRewards(Player unit)
        {
            foreach(var reward in QuestRewards)
            {
                unit.AddGold(reward.Gold);

                if (reward.ItemTypeID > 0 && (reward.CharacterTypeID == 0 || reward.CharacterTypeID == unit.CharacterTypeID))
                {
                    InventoryItem item = LevelHandler.InventoryTemplate(reward.ItemTypeID, reward.ItemQuantity);

                    unit.AddInventory(item);
                }
            }
        }
    }

    public class QuestKillCount
    {
        public int QuestID { get; set; }
        public int CharacterID { get; set; }
        public int EnemyID { get; set; }
        public int Quantity { get; set; }
    }

    public class QuestTalkCount
    {
        public int QuestID { get; set; }
        public int CharacterID { get; set; }
        public int NPCID { get; set; }
        public int Quantity { get; set; }
    }

    public class QuestReward
    {
        public int Gold { get; set; }
        public int ItemTypeID { get; set; }
        public int CharacterTypeID { get; set; }
        public int ItemQuantity { get; set; }
    }

    public class QuestRequirement
    {
        public enum QuestRequirementTypeEnum
        {
            Fetch = 1,
            Hunt = 2,
            Talk = 3
        }

        public QuestRequirementTypeEnum RequirementType { get; set; }
        public int HuntEnemyTypeID { get; set; }
        public int HuntEnemyQuantity { get; set; }
        public int FetchItemID { get; set; }
        public int FetchItemQuantity { get; set; }
        public int TalkNPCID { get; set; }

        internal bool CheckRequirement(Player unit, int questID)
        {
            bool result = true;

            switch (RequirementType)
            {
                case QuestRequirementTypeEnum.Fetch:
                    result = unit.InventoryItemQty(FetchItemID) >= FetchItemQuantity;
                    break;
                case QuestRequirementTypeEnum.Hunt:
                    var killCount = unit.QuestKillCounts.FirstOrDefault(qkc => qkc.QuestID == questID && qkc.EnemyID == HuntEnemyTypeID);
                    if (killCount != null)
                        return killCount.Quantity >= HuntEnemyQuantity;
                    break;
                case QuestRequirementTypeEnum.Talk:
                    var talkCount = unit.QuestTalkCounts.FirstOrDefault(qtc => qtc.QuestID == questID && qtc.NPCID == TalkNPCID);
                    if (talkCount != null)
                        return talkCount.Quantity > 0;
                    break;
            }

            return result;
        }

        internal void UpdateInventory(Player unit)
        {
            if (RequirementType == QuestRequirementTypeEnum.Fetch)
            {
                for (int index = 0; index < FetchItemQuantity; index++)
                {
                    unit.ReduceInventory(FetchItemID);
                }
            }
        }
    }

    public class QuestCondition
    {
        public List<int> Quests { get; set; }
        public int CharacterMinimumLevel { get; set; }
        public int CharacterType { get; set; }
    }
}
