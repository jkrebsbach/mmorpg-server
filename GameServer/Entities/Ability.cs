﻿using GameServer.Math;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameServer.Entities
{
    public class Ability
    {
        public enum DamageTypeEnum
        {
            NotApplicable = 0,
            Physical = 1,
            Magical = 2
        }

        public enum ElementalEnum
        {
            None = 0,
            Fire = 1,
            Ice = 2
        }

        #region Public Properties
        public DamageTypeEnum DamageType;
        public string ResistTarget;
        public string OffsetCalc;
        public string OffsetTarget;
        public int PercentOffset;
        public int Ratio;
        public int InitialHitChanceIncrease;
        public int RoundTwoDamagePercent;
        public int ExtraDropRoll;
        public bool BackRow;
        public bool StrikeTwice;
        public decimal ShopCostDecrease;
        public int AbilityCostPercent;
        public int ItemReusePercent;
        public int DefaultDuration;
        public bool RangedWeapon;
        
        public int AbilityID { get; private set; }

        public String AbilityName { get; private set; }

        public int Level { get; private set; }

        public int Strength { get; private set; }

        public int Defense { get; private set; }

        public int Cost { get; private set; }

        public List<int> CharacterTypeID { get; private set; }

        public bool Active { get; private set; }

        public ElementalEnum Element { get; private set; }

        public List<Buff> Modifiers { get; private set; }

        #endregion

        public Ability(int abilityID, string abilityName, int level, int strength, int defense,
            int cost, bool active, List<int> characterTypeID, ElementalEnum element, List<Buff> modifiers)
        {
            AbilityID = abilityID;
            AbilityName = abilityName;
            Level = level;
            Strength = strength;
            Defense = defense;
            Cost = cost;
            Active = active;
            CharacterTypeID = characterTypeID;
            Element = element;

            //_targetModifiers = modifiers.Where(a => a.CasterEffect == false).ToList();
            //_casterModifiers = modifiers.Where(a => a.CasterEffect == true).ToList();
            Modifiers = modifiers;
        }

        public void CastAbility(GameObject target, GameObject caster)
        {
            if (target == null)
                return;

            var abilitySuccess = caster.CheckEnergy(-Cost);
            if (abilitySuccess)
            {
                if (RangedWeapon && caster.EquipRanged <= 0)
                    return;

                target.ResolveEffect(this, caster);
                caster.ResolveCasting(this);
            }
        }

        /// <summary>
        /// Calculate damage for physical attack
        /// </summary>
        /// <param name="target"></param>
        /// <param name="caster"></param>
        internal static void CastAttack(GameObject target, GameObject caster, List<Buff> attackBuffs)
        {
            var hit = MathLibrary.RangeTester(target.Agility - target.Equipment.Sum(eq => eq.Weight), caster.Agility);
            
            if (hit)
            {
                double critPcnt = MathLibrary.Random(100) + 1; // 1 to 100

                double strength = caster.Strength + caster.Equipment.Sum(eq => eq.Strength);

                // If we are in backrow, unbuffed - reduce damage applied
                if (attackBuffs == null || !attackBuffs.Any(ab => ab.BackRow))
                    strength = (caster.FrontRow ? strength : (int)((double)strength * .75));

                double critChance = 10; // 10% chance to crit
                if (critPcnt <= critChance)
                    strength *= 2;

                strength += ((double)(strength * critPcnt) / 1000.0); // 100 = 10% up, 50 = 5% up, 0 = 0% up

                
                double defense = target.Defense + target.Equipment.Sum(eq => eq.Defense);
                defense = (target.FrontRow ? defense : (int)(defense * 1.25));


                var damage = (int)(strength - defense);

                if (damage < 0)
                    damage = 0;

                target.TakeDamage(damage);
            }
            else
            {
                target.TakeDamage(0);
            }

            int cooldown = 4;
            if (target.CurrentBattle != null)
            {
                double baseAgility = (double)caster.Agility / (double)target.CurrentBattle.GameObjects.Values.Where(go => go.Ally != caster.Ally).Average(ag => ag.Agility);

                if (baseAgility < .7)
                    cooldown = 6;
                else if (baseAgility < .9)
                    cooldown = 5;
                else if (baseAgility < 1.1)
                    cooldown = 4;
                else if (baseAgility < 1.3)
                    cooldown = 3;
                else
                    cooldown = 2;
            }

            caster.UpdateCoolDown(cooldown);
        }

        internal List<Buff> CalculateBuff(GameObject caster, GameObject target)
        {
            var result = new List<Buff>();

            if (BackRow)
                result.Add(new Buff() { BackRow = true });

            if (!String.IsNullOrEmpty(ResistTarget))
            {
                int casterVal = 0, targetVal = 0;
                switch (ResistTarget)
                {
                    case "defense":
                        casterVal = caster.Defense;
                        targetVal = target.Defense;
                        break;
                    case "strength":
                        casterVal = caster.Strength;
                        targetVal = target.Strength;
                        break;
                    case "agility":
                        casterVal = caster.Agility;
                        targetVal = target.Agility;
                        break;
                    case "charisma":
                        casterVal = caster.Charisma;
                        targetVal = target.Charisma;
                        break;
                    case "willpower":
                        casterVal = caster.Willpower;
                        targetVal = target.Willpower;
                        break;
                    case "intelligence":
                        casterVal = caster.Intelligence;
                        targetVal = target.Intelligence;
                        break;
                }

                if (!MathLibrary.RangeTester(casterVal, targetVal))
                    return result; // Successfully resisted
            }

            if (!String.IsNullOrEmpty(OffsetCalc))
            {
                int casterVal = 0;
                switch (OffsetTarget)
                {
                    case "defense":
                        casterVal = caster.Defense;
                        break;
                    case "strength":
                        casterVal = caster.Strength;
                        break;
                    case "agility":
                        casterVal = caster.Agility;
                        break;
                    case "charisma":
                        casterVal = caster.Charisma;
                        break;
                    case "willpower":
                        casterVal = caster.Willpower;
                        break;
                    case "intelligence":
                        casterVal = caster.Intelligence;
                        break;
                }

                var buff = new Buff {DefaultDuration = DefaultDuration};
                if (DefaultDuration == 0)
                    buff.Expiry = DateTime.MaxValue;
                else
                    buff.Expiry = DateTime.UtcNow.AddSeconds(DefaultDuration);

                if (PercentOffset != 0)
                {
                    var offset =  (int)((double)casterVal * (double)PercentOffset / 100.0);

                    switch (OffsetCalc)
                    {
                        case "defense":
                            buff.DefenseOffset = offset;
                            break;
                        case "strength":
                            buff.StrengthOffset = offset;
                            break;
                        case "agility":
                            buff.AgilityOffset = offset;
                            break;
                        case "charisma":
                            buff.CharismaOffset = offset;
                            break;
                        case "willpower":
                            buff.WillpowerOffset = offset;
                            break;
                        case "intelligence":
                            buff.IntelligenceOffset = offset;
                            break;
                    }
                }

                if (Ratio != 0)
                {
                    int ratio = casterVal * Ratio;

                    switch (OffsetCalc)
                    {
                        case "cooldown":
                            target.UpdateCoolDown(ratio);
                            return null;
                        case "health":
                            target.TakeDamage(ratio);
                            return null;
                    }
                }

                result.Add(buff);
            }

            if (AbilityCostPercent != 0)
                caster.UpdateCosts(AbilityCostPercent, ItemReusePercent, DefaultDuration);

            return result;
        }
    }
}
