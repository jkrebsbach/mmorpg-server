﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace GameServer.Entities
{
    public class LevelHandler
    {
        private List<GameObject> _gameObjects; // Who is currently in the world
        private Dictionary<Guid, BattleHandler> _gameBattles; // What battles are currently ongoing
        private Dictionary<int, Location> _locations; // Towns, dungeons, etc...
        private Dictionary<int, Enemy> _enemyLibrary;
        private static Dictionary<int, Dictionary<int, Ability>> _abilityLibrary;
        private static Dictionary<int, InventoryItem> _itemLibrary;
        private static Dictionary<int, CharacterType> _characterTypeLibrary;
        private Dictionary<int, Quest> _questLibrary;
        
        public List<GameObject> GameObjects
        {
            get
            {
                return _gameObjects;
            }
        }

        public Dictionary<Guid, BattleHandler> GameBattles
        {
            get
            {
                return _gameBattles;
            }
        }

        public Dictionary<int, Location> Locations
        {
            get
            {
                return _locations;
            }
        }

        public Dictionary<int, Quest> QuestLibrary
        {
            get
            {
                return _questLibrary;
            }
        }

        public GameObject EnemyTemplate(int enemyID)
        {
            return _enemyLibrary[enemyID];
        }

        public static Ability AbilityTemplate(int abilityID, int level)
        {
            return _abilityLibrary[abilityID][level];
        }

        public static InventoryItem InventoryTemplate(int itemID, int quantity)
        {
            var itemTemplate = _itemLibrary[itemID];
            var item = new InventoryItem(quantity, itemID, itemTemplate.AbilityName, itemTemplate.Strength,
                itemTemplate.Defense, itemTemplate.Cost, itemTemplate.ItemType, itemTemplate.Weight, itemTemplate.CharacterTypeID, itemTemplate.Element, itemTemplate.Modifiers);

            item.OffsetCalc = itemTemplate.OffsetCalc;
            item.OffsetTarget = itemTemplate.OffsetTarget;
            item.PercentOffset = itemTemplate.PercentOffset;
            item.DefaultDuration = itemTemplate.DefaultDuration;

            return item;
        }

        public static CharacterType CharacterTypeTemplate(int characterType)
        {
            return _characterTypeLibrary[characterType];
        }

        public LevelHandler(GameServer gameServer)
        {
            _gameObjects = new List<GameObject>();
            _gameBattles = new Dictionary<Guid, BattleHandler>();
            _locations = new Dictionary<int, Location>();

            _abilityLibrary = LoadAbilityLibrary();
            _itemLibrary = LoadItemLibrary();
            _enemyLibrary = LoadEnemyLibrary();
            _questLibrary = LoadQuestLibrary();
            _locations = LoadLocationLibrary();
            _characterTypeLibrary = LoadCharacterTypes();

            CheckBattleCounts(gameServer);
        }

        public void RemovePlayer(GameObject player)
        {
            _gameObjects.Remove(player);
            foreach(BattleHandler gameBattle in _gameBattles.Values)
            {
                gameBattle.LeaveBattle(player);
            }
        }

        private Dictionary<int, Location> LoadLocationLibrary()
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load("locationList.xml");

            Dictionary<int, Location> result = new Dictionary<int, Location>();

            foreach (XmlNode xmlLocation in xmlDoc.DocumentElement.ChildNodes)
            {
                int locationID = int.Parse(xmlLocation.Attributes["id"].Value);
                string resourceName = xmlLocation.Attributes["resource"].Value;
                float overworldX = float.Parse(xmlLocation.Attributes["overworldX"].Value);
                float overworldY = float.Parse(xmlLocation.Attributes["overworldY"].Value);
                float startX = float.Parse(xmlLocation.Attributes["startX"].Value);
                float startY = float.Parse(xmlLocation.Attributes["startY"].Value);

                var location = GenerateLocation(locationID, new Math.Vector2(overworldX, overworldY), new Math.Vector2(startX, startY), resourceName);
                result[location.LocationID] = location;

                foreach(XmlNode xmlNPC in xmlLocation["npcs"])
                {
                    int npcID = int.Parse(xmlNPC.Attributes["id"].Value);
                    int sceneID = int.Parse(xmlNPC.Attributes["scene"].Value);
                    float npcLocationX = float.Parse(xmlNPC.Attributes["locationX"].Value);
                    float npcLocationY = float.Parse(xmlNPC.Attributes["locationY"].Value);

                    location.NPCs.Add(new NPC(npcID, location.LocationID, sceneID, new Math.Vector2(npcLocationX, npcLocationY)));
                }
            }
            
            
            return result;
        }

        private Dictionary<int, CharacterType> LoadCharacterTypes()
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load("playerDatabase.xml");

            Dictionary<int, CharacterType> result = new Dictionary<int, CharacterType>();

            foreach (XmlNode xmlLocation in xmlDoc.DocumentElement.ChildNodes)
            {
                int id = int.Parse(xmlLocation.Attributes["id"].Value);

                var characterType = new CharacterType();
                characterType.ExperienceLevels = new Dictionary<int, int>();

                result[id] = characterType;

                foreach (XmlNode xmlNPC in xmlLocation["levels"])
                {
                    int levelID = int.Parse(xmlNPC.Attributes["id"].Value);
                    int experience = int.Parse(xmlNPC.Attributes["quantity"].Value);

                    characterType.ExperienceLevels[levelID] = experience;
                }
            }


            return result;
        }

        private Dictionary<int, Enemy> LoadEnemyLibrary()
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load("enemyList.xml");

            Dictionary<int, Enemy> result = new Dictionary<int, Enemy>();

            foreach(XmlNode enemy in xmlDoc.DocumentElement.ChildNodes)
            {
                int id = int.Parse(enemy.Attributes["id"].Value);
                string textureName = enemy.Attributes["textureName"].Value;
                string characterName = enemy.Attributes["characterName"].Value;
                int health = int.Parse(enemy.Attributes["hitPoints"].Value);
                int energy = int.Parse(enemy.Attributes["energy"].Value);

                int defense = int.Parse(enemy.Attributes["Defense"].Value);
                int strength = int.Parse(enemy.Attributes["Strength"].Value);
                int agility = int.Parse(enemy.Attributes["Agility"].Value);
                int charisma = int.Parse(enemy.Attributes["Charisma"].Value);
                int willpower = int.Parse(enemy.Attributes["Willpower"].Value);
                int intelligence = int.Parse(enemy.Attributes["Intelligence"].Value);
                string ai = enemy.Attributes["ai"].Value;

                Dictionary<int, Ability> abilities = new Dictionary<int, Ability>();

                foreach (XmlNode ability in enemy["abilities"].ChildNodes)
                {
                    int abilityID = int.Parse(ability.Attributes["id"].Value);
                    int level = int.Parse(ability.Attributes["level"].Value);
                    abilities[abilityID] = _abilityLibrary[abilityID][level];
                }

                XmlNode drops = enemy["drops"];

                int gold = int.Parse(drops.Attributes["gold"].Value);
                int experience = int.Parse(drops.Attributes["experience"].Value);

                CharacterModel character = new CharacterModel()
                {
                    CharacterName = characterName,
                    MaxHealth = health,
                    MaxEnergy = energy,
                    Strength = strength,
                    Defense = defense,
                    Agility = agility,
                    Charisma = charisma,
                    Willpower = willpower,
                    Intelligence = intelligence
                };

                Enemy enemyObj = new Enemy((GameObject.AICategory)Enum.Parse(typeof(GameObject.AICategory), ai),
                    textureName, character, false, abilities, new Dictionary<int, InventoryItem>());

                enemyObj.EnemyID = id;
                enemyObj.Gold = gold;
                enemyObj.Experience = experience;
                
                foreach (XmlNode item in drops.ChildNodes)
                {
                    int inventoryItemID = int.Parse(item.Attributes["itemTypeID"].Value);
                    double dropFrequency = double.Parse(item.Attributes["frequency"].Value);

                    enemyObj.Drops.Add(new EnemyDrop()
                        {
                            InventoryItemID = inventoryItemID,
                            DropFrequency = dropFrequency
                        });
                }

                result[id] = enemyObj;

            }

            return result;
        }

        private Dictionary<int, InventoryItem> LoadItemLibrary()
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load("itemLibrary.xml");

            Dictionary<int, InventoryItem> result = new Dictionary<int, InventoryItem>();

            foreach (XmlNode ability in xmlDoc.DocumentElement.ChildNodes)
            {
                int abilityID = int.Parse(ability.Attributes["id"].Value);
                string abilityName = ability.Attributes["name"].Value;
                int strength = int.Parse(ability.Attributes["strength"].Value);
                int defense = int.Parse(ability.Attributes["defense"].Value);
                int weight = int.Parse(ability.Attributes["weight"].Value);
                int cost = int.Parse(ability.Attributes["cost"].Value);
                string itemTypeText = ability.Attributes["type"].Value;

                string offsetCalc = null;
                string offsetTarget = null;
                int percentOffset = 0;
                int defaultDuration = 0;
                if (ability.Attributes["offsetCalc"] != null)
                {
                    offsetCalc = ability.Attributes["offsetCalc"].Value;
                    offsetTarget = ability.Attributes["offsetTarget"].Value;
                    percentOffset = int.Parse(ability.Attributes["percentOffset"].Value);
                    defaultDuration = int.Parse(ability.Attributes["defaultDuration"].Value);
                }

                InventoryItem.ItemTypeEnum itemType = (InventoryItem.ItemTypeEnum)Enum.Parse(typeof(InventoryItem.ItemTypeEnum), itemTypeText);
                Ability.ElementalEnum element = Ability.ElementalEnum.None;
                if (ability.Attributes["element"] != null)
                {
                    string elementText = ability.Attributes["element"].Value;
                    element = (Ability.ElementalEnum)Enum.Parse(typeof(Ability.ElementalEnum), elementText);
                }

                XmlElement item = ability["charactertypes"];

                List<int> characterTypes = new List<int>();
                if (item != null)
                {
                    foreach (XmlElement characterType in item)
                    {
                        characterTypes.Add(int.Parse(characterType.Attributes["id"].Value));
                    }
                }

                List<Buff> modifiers = new List<Buff>();

                var inventoryItem = new InventoryItem(0, abilityID, abilityName, strength, defense, cost, itemType, weight, characterTypes, element, modifiers);
                inventoryItem.OffsetCalc = offsetCalc;
                inventoryItem.OffsetTarget = offsetTarget;
                inventoryItem.PercentOffset = percentOffset;
                inventoryItem.DefaultDuration = defaultDuration;
                
                result[abilityID] = inventoryItem;
            }

            return result;
        }

        private Dictionary<int, Dictionary<int, Ability>> LoadAbilityLibrary()
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load("abilityList.xml");

            Dictionary<int, Dictionary<int, Ability>> result = new Dictionary<int, Dictionary<int, Ability>>();

            foreach (XmlNode xmlAbility in xmlDoc.DocumentElement.ChildNodes)
            {
                int abilityID = int.Parse(xmlAbility.Attributes["id"].Value);
                string abilityName = xmlAbility.Attributes["name"].Value;
                int strength = 0;
                if (xmlAbility.Attributes["strength"] != null)
                    strength = int.Parse(xmlAbility.Attributes["strength"].Value);
                bool active = int.Parse(xmlAbility.Attributes["active"].Value) == 1;
                int characterTypeID = int.Parse(xmlAbility.Attributes["charactertypeid"].Value);
                List<int> characterTypes = new List<int>() { characterTypeID };

                result[abilityID] = new Dictionary<int, Ability>();

                foreach(XmlNode xmlLevel in xmlAbility["levels"].ChildNodes)
                {
                    
                    int level = int.Parse(xmlLevel.Attributes["id"].Value);
                    int cost = 0;

                    if (xmlLevel.Attributes["cost"] != null)
                        cost = int.Parse(xmlLevel.Attributes["cost"].Value);

                    Ability.ElementalEnum element = Ability.ElementalEnum.None;
                    if (xmlAbility["levels"].Attributes["element"] != null)
                        element = (Ability.ElementalEnum)Enum.Parse(typeof(Ability.ElementalEnum), xmlAbility["levels"].Attributes["element"].Value);
                    
                    var ability = new Ability(abilityID, abilityName, level, strength, 0, cost, active, characterTypes, element, new List<Buff>());

                    if (xmlAbility.Attributes["damagetype"] != null)
                        ability.DamageType = (Ability.DamageTypeEnum)(int.Parse(xmlAbility.Attributes["damagetype"].Value));

                    if (xmlAbility["levels"].Attributes["backRow"] != null)
                        ability.BackRow = true;
                    if (xmlAbility.Attributes["strikeTwice"] != null)
                        ability.StrikeTwice = true;
                    if (xmlAbility["levels"].Attributes["defaultDuration"] != null)
                        ability.DefaultDuration = int.Parse(xmlAbility["levels"].Attributes["defaultDuration"].Value);
                    if (xmlAbility["levels"].Attributes["resistTarget"] != null)
                        ability.ResistTarget = xmlAbility["levels"].Attributes["resistTarget"].Value;
                    if (xmlAbility["levels"].Attributes["offsetCalc"] != null)
                        ability.OffsetCalc = xmlAbility["levels"].Attributes["offsetCalc"].Value;
                    if (xmlAbility["levels"].Attributes["offsetTarget"] != null)
                        ability.OffsetTarget = xmlAbility["levels"].Attributes["offsetTarget"].Value;
                    if (xmlLevel.Attributes["percentOffset"] != null)
                        ability.PercentOffset = int.Parse(xmlLevel.Attributes["percentOffset"].Value);
                    if (xmlLevel.Attributes["ratio"] != null)
                        ability.Ratio = int.Parse(xmlLevel.Attributes["ratio"].Value);
                    if (xmlLevel.Attributes["initialHitChanceIncrease"] != null)
                        ability.InitialHitChanceIncrease = int.Parse(xmlLevel.Attributes["initialHitChanceIncrease"].Value);
                    if (xmlLevel.Attributes["roundTwoDamagePercent"] != null)
                        ability.RoundTwoDamagePercent = int.Parse(xmlLevel.Attributes["roundTwoDamagePercent"].Value);
                    if (xmlLevel.Attributes["extraDropRoll"] != null)
                        ability.ExtraDropRoll = int.Parse(xmlLevel.Attributes["extraDropRoll"].Value);
                    if (xmlLevel.Attributes["shopCostDecrease"] != null)
                        ability.ShopCostDecrease = int.Parse(xmlLevel.Attributes["shopCostDecrease"].Value);
                    if (xmlLevel.Attributes["abilityCostPercent"] != null)
                        ability.AbilityCostPercent = int.Parse(xmlLevel.Attributes["abilityCostPercent"].Value);
                    if (xmlLevel.Attributes["itemReusePercent"] != null)
                        ability.ItemReusePercent = int.Parse(xmlLevel.Attributes["itemReusePercent"].Value);
                    if (xmlAbility.Attributes["rangedweapon"] != null)
                        ability.RangedWeapon = true;


                    // TODO: Convert abilities into buffs
                    List<Buff> modifiers = new List<Buff>();
                    var buff = new Buff();
                    modifiers.Add(buff);

                    
                    result[abilityID][level] = ability;
                }
                
            }

            return result;
        }

        private Dictionary<int, Quest> LoadQuestLibrary()
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load("questList.xml");

            Dictionary<int, Quest> result = new Dictionary<int, Quest>();

            foreach (XmlNode ability in xmlDoc.DocumentElement.ChildNodes)
            {
                int questID = int.Parse(ability.Attributes["id"].Value);
                string questName = ability.Attributes["name"].Value;
                
                XmlElement requirements = ability["requirements"];
                XmlElement conditions = ability["availabilityConditions"];
                XmlElement rewards = ability["rewards"];

                result[questID] = new Quest()
                {
                    QuestID = questID,
                    Description = questName
                };

                if (requirements != null)
                {
                    foreach(XmlNode requirement in requirements.ChildNodes)
                    {
                        QuestRequirement.QuestRequirementTypeEnum type = 
                            (QuestRequirement.QuestRequirementTypeEnum)Enum.Parse(typeof(QuestRequirement.QuestRequirementTypeEnum), requirement.Attributes["requirementType"].Value);

                        int fetchItemID = 0;
                        int fetchItemQty = 0;
                        int huntEnemyID = 0;
                        int huntEnemyQty = 0;
                        int talkNPCID = 0;

                        if (requirement.Attributes["fetchItemID"] != null)
                            fetchItemID = int.Parse(requirement.Attributes["fetchItemID"].Value);
                        if (requirement.Attributes["fetchItemQuantity"] != null)
                            fetchItemQty = int.Parse(requirement.Attributes["fetchItemQuantity"].Value);
                        if (requirement.Attributes["huntEnemyID"] != null)
                            huntEnemyID = int.Parse(requirement.Attributes["huntEnemyID"].Value);
                        if (requirement.Attributes["huntEnemyQuantity"] != null)
                            huntEnemyQty = int.Parse(requirement.Attributes["huntEnemyQuantity"].Value);
                        if (requirement.Attributes["talkNPCID"] != null)
                            talkNPCID = int.Parse(requirement.Attributes["talkNPCID"].Value);

                        result[questID].QuestRequirements.Add(new QuestRequirement() {
                            RequirementType = type,
                            FetchItemID = fetchItemID,
                            FetchItemQuantity = fetchItemQty,
                            HuntEnemyTypeID = huntEnemyID,
                            HuntEnemyQuantity = huntEnemyQty,
                            TalkNPCID = talkNPCID
                        });
                    }
                }

                if (conditions != null)
                {
                    List<int> questIDs = new List<int>();
                    int minLevel = 0;
                    int characterType = 0;

                    XmlElement quests = conditions["quests"];
                    if (quests != null)
                    {
                        foreach(XmlNode quest in quests.ChildNodes)
                        {
                            questIDs.Add(int.Parse(quest.Attributes["questID"].Value));
                        }
                    }

                    XmlElement character = conditions["character"];
                    if (character != null)
                    {
                        minLevel = int.Parse(character.Attributes["minimumLevel"].Value);
                        if (character.Attributes["characterTypeID"] != null)
                            characterType = int.Parse(character.Attributes["characterTypeID"].Value);
                    }

                    result[questID].QuestCondition = new QuestCondition()
                    {
                        Quests = questIDs,
                        CharacterMinimumLevel = minLevel,
                        CharacterType = characterType
                    };
                }

                if (rewards != null)
                {
                    foreach(XmlNode reward in rewards.ChildNodes)
                    {
                        int gold = 0;
                        int itemTypeID = 0;
                        int itemQuantity = 0;
                        int characterTypeID = 0;

                        if (reward.Attributes["gold"] != null)
                            gold = int.Parse(reward.Attributes["gold"].Value);
                        if (reward.Attributes["itemTypeID"] != null)
                            itemTypeID = int.Parse(reward.Attributes["itemTypeID"].Value);
                        if (reward.Attributes["itemQuantity"] != null)
                            itemQuantity = int.Parse(reward.Attributes["itemQuantity"].Value);
                        if (reward.Attributes["characterTypeID"] != null)
                            characterTypeID = int.Parse(reward.Attributes["characterTypeID"].Value);
                        

                        result[questID].QuestRewards.Add(new QuestReward() { 
                            Gold = gold,
                            ItemTypeID = itemTypeID,
                            CharacterTypeID = characterTypeID,
                            ItemQuantity = itemQuantity
                        });
                    }
                }

                
            }

            return result;
        }

        public BattleHandler GenerateBattle(List<int> enemyTemplates, List<GameObject> allies, 
            GameServer gameServer, int locationID, int sceneID, Math.Vector2 overworldPosition)
        {
            List<GameObject> combatants = new List<GameObject>();
            enemyTemplates.ForEach(et => combatants.Add(new Enemy(_enemyLibrary[et])));

            combatants.AddRange(allies);

            BattleHandler battleHandler = new BattleHandler(combatants, gameServer, locationID, sceneID, overworldPosition);
            return battleHandler;
        }

        public Location GenerateLocation(int locationID, Math.Vector2 location, Math.Vector2 start, string resourceName)
        {
            return new Location(locationID, location, start, resourceName);
        }

        internal void CheckBattleCounts(GameServer server)
        {
            // 1 - Pig
            // 2 - Squirrel
            // 3 - Spider
            // 4 - Gecko
            // 5 - Hornet
            // 6 - Dragon
            // 7 - Goblin

            // Enemies in overworld
            CheckSceneBattle(server, 0, -1, 3, new List<int>() { 1, 2 }, new Math.Vector2(-6f, -2f));

            // Enemies in forest
            CheckSceneBattle(server, 4, 0, 3, new List<int>() { 3, 4 }, new Math.Vector2(-4f, -2f));
            CheckSceneBattle(server, 4, 1, 3, new List<int>() { 3, 4 }, new Math.Vector2(-4f, -2f));
            CheckSceneBattle(server, 4, 2, 3, new List<int>() { 3, 4 }, new Math.Vector2(-4f, -2f));
            CheckSceneBattle(server, 4, 3, 3, new List<int>() { 3, 4 }, new Math.Vector2(-4f, -2f));

            // Enemies in cave
            CheckSceneBattle(server, 5, 0, 3, new List<int>() { 5, 6 }, new Math.Vector2(-4f, -2f));
            CheckSceneBattle(server, 5, 1, 3, new List<int>() { 5, 6 }, new Math.Vector2(-4f, -2f));
            
            // Enemies deeper in cave
            CheckSceneBattle(server, 5, 2, 3, new List<int>() { 7, 7 }, new Math.Vector2(-4f, -2f));
            CheckSceneBattle(server, 5, 3, 3, new List<int>() { 7, 7 }, new Math.Vector2(-4f, -2f));
        }

        private void CheckSceneBattle(GameServer server, int locationID, int sceneID, int targetCount, List<int> enemyTypes, Math.Vector2 spawnPoint)
        {
            // Enemies deeper in cave
            while (_gameBattles.Values.Where(gb => gb.LocationID == locationID && gb.Scene == sceneID).Count() < targetCount)
            {
                // Create our default battle for testing
                var battleHandler = GenerateBattle(enemyTypes, new List<GameObject>(), server, locationID, sceneID, spawnPoint);

                _gameBattles[battleHandler.BattleID] = battleHandler;
            }
        }
    }
}
