﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameServer.Entities
{
    public class Enemy : GameObject
    {
        public int EnemyID;
        public int Experience;
        public int Gold;

        public Enemy(AICategory ai, string textureName, CharacterModel character, 
            bool ally, Dictionary<int, Ability> abilities, Dictionary<int, InventoryItem> inventory) :
            base (ai, null, textureName, character, ally, abilities, inventory)
        { }

        public Enemy(Enemy template) : base(template)
        {
            EnemyID = template.EnemyID;
            Experience = template.Experience;
            Gold = template.Gold;
            Drops = template.Drops;
        }

        public List<EnemyDrop> Drops = new List<EnemyDrop>();
    }

    public class EnemyDrop
    {
        public int InventoryItemID;
        public double DropFrequency;
    }
}
