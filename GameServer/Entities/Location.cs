﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameServer.Entities
{
    public class Location
    {
        private int _locationID;
        private Math.Vector2 _overworldPosition;
        private Math.Vector2 _playerStart;
        private string _locationResource;
        public List<NPC> _npcs;

        public int LocationID
        {
            get
            {
                return _locationID;
            }
        }

        public Math.Vector2 OverworldPosition
        {
            get
            {
                return _overworldPosition;
            }
        }

        public Math.Vector2 PlayerStart
        {
            get
            {
                return _playerStart;
            }
        }

        public string LocationResource
        {
            get
            {
                return _locationResource;
            }
        }

        public List<NPC> NPCs
        {
            get
            {
                return _npcs;
            }
        }

        public Location(int locationID, Math.Vector2 position, Math.Vector2 start, string resource)
        {
            _locationID = locationID;
            _overworldPosition = position;
            _playerStart = start;
            _locationResource = resource;

            _npcs = new List<NPC>();
        }
    }
}
