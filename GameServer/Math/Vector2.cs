﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameServer.Math
{
    public class Vector2
    {
        public float x;
        public float y;

        public Vector2(float intX, float intY)
        {
            x = intX;
            y = intY;
        }

        /// <summary>
        /// Check to see if vector is within a given magnitude of target point
        /// Square is more efficient than square root
        /// </summary>
        /// <param name="comparison"></param>
        /// <param name="threshhold"></param>
        /// <returns></returns>
        public bool Overlap(Vector2 comparison, float threshhold)
        {
            float xd = this.x - comparison.x;
	        float yd = this.y - comparison.y;

            return (threshhold * threshhold) > (xd * xd + yd * yd);
        }

        public Vector2 Add(Vector2 operand)
        {
            float xd = this.x + operand.x;
            float yd = this.y + operand.y;

            return new Vector2(xd, yd);
        }

        public Vector2 Subtract(Vector2 operand)
        {
            float xd = this.x - operand.x;
            float yd = this.y - operand.y;

            return new Vector2(xd, yd);
        }

        public Vector2 Times(float operand)
        {
            float xd = this.x * operand;
            float yd = this.y * operand;

            return new Vector2(xd, yd);
        }

        public Vector2 Normalize()
        {
            float distance = (float)System.Math.Sqrt(x*x + y*y);
            return new Vector2(x / distance, y / distance);
        }

        public double Magnitude
        {
            get { return System.Math.Sqrt(x*x+y*y); }
        }
    }
}
