﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameServer.Math
{
    public static class MathLibrary
    {
        private static Random rnd = new Random();

        public static bool RangeTester(int targetValue, int casterValue)
        {
            double rangePcnt = rnd.Next(76) + 25; // 25 to 100

            return casterValue >= (targetValue * (rangePcnt / 100.0));
        }

        /// <summary>
        /// Returns a non-negative random number less than the specified maximum
        /// </summary>
        /// <param name="next"></param>
        /// <returns></returns>
        public static int Random(int next)
        {
            return rnd.Next(next);
        }
    }
}
