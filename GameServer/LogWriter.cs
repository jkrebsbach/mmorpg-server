﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Tracing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameServer
{
    public class LogWriter : EventSource
    {
        public static LogWriter Log = new LogWriter();

        public void WriteMessage(string message)
        {
            LogError(2, "MISC", message);
        }

        public void LogError(int level, string category, string message)
        {
            Console.WriteLine("[" + level + ", " + category + "] " + message);

            if (IsEnabled())
                WriteEvent(2, message);
            
        }

        public void DebugMessage(string category, string message)
        {
            Console.WriteLine("[" + category + "] " + message);
        }
    }
}
