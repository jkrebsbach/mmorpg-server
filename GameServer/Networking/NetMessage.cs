﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameServer.Networking
{
    public class NetMessage
    {
        private List<Object> _data;
        
        public NetMessage()
        {
            _data = new List<object>();
        }

        public void Write(Object data)
        {
            _data.Add(data);
        }

        public void SendMessage(ISocketConnection socket, NetDeliveryMethod deliveryMethod)
        {
            StringBuilder dataPacket = new StringBuilder();

            foreach(Object data in _data)
            {
                dataPacket.Append(data.ToString() + "|");
            }

            socket.SendString(dataPacket.ToString());
        }
    }

    public enum NetDeliveryMethod
    {
        ReliableOrdered
    }

    public enum NetworkPackageFromClient
        {
            // Game Server Packages              1xxx
            MOTD = 1001,
            WhoAmI = 1002,
            WhoIsPlayer = 1003,
            ClientChat = 1004,
            LearnSkill = 1005,
            Logout = 1006,
            
            // BattleHandler Update Package  2xxx
            AttackInitiate = 2001,
            SpellInitiate = 2002,
            InventoryInitiate = 2003,
            AttemptDefend = 2004,
            AttemptEscape = 2005,
            LeaveBattle = 2006,
            SwitchRow = 2007,
            
            // Overworld activity
            LoadLocations = 3001,
            CharacterMovement = 3002,
            UpdateEquipment = 3003,
            EnterBattle = 3004,
            EnterLocation = 3005,
            Respawn = 3006,

            // Location activity
            LeaveLocation = 4001,
            StoreInventory = 4002,
            QuestSelection = 4003,
            StartQuest = 4004,
            PurchaseItem = 4005,
            CompleteQuest = 4006,
            StartDialog = 4007,
            SellItem = 4008
        }

        public enum NetworkPackageToClient
        {
            // Game Server Packages              1xxx
            MOTD = 1001,
            WhoAmI = 1002,
            WhoIsPlayer = 1003,
            ChatMessage = 1004,
            ShutDown = 1005,
            SkillLearned = 1006,
            
            // GameObject("any") BattleHandler Update Package 2xxx
            UpdateBattleHandler = 2001,
            DestroyBattleHandler = 2002,
            UpdateBattleEntity = 2003,
            DestroyBattleEntity = 2004,
            InitiateGameObject = 2005,
            DestroyGameObject = 2006,
            UpdateHealth = 2007,
            UpdateCoolDown = 2008,
            UpdateSpells = 2009,
            UpdateInventory = 2010,
            EscapeAttempt = 2011,
            InitiateAbility = 2012,
            BattleReward = 2013,
            
            // Overworld activity
            UpdateLocationHandler = 3001,
            CharacterMovement = 3002,
            NPCMovement = 3003,
            Respawn = 3004,

            // Location activity
            StoreInventory = 4001,
            QuestSelection = 4002,
            DialogSelection = 4003
        }
}
