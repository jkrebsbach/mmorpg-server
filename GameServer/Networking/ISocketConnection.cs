﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameServer.Networking
{
    public interface ISocketConnection
    {
        string SenderConnection { get; }

        void Shutdown();
        void SendString(string data);
        void RegisterSocket();
    }
}
