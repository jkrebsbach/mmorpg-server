﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace GameServer.Networking
{
    public class GameSocket : ISocketConnection
    {
        public event EventHandler<GameSocketEventArgs> MessageReceived;
        public event EventHandler ClientDisconnected;

        public class GameSocketEventArgs
        {
            public string Message;
        }

        private Socket _socket;

        public GameSocket(Socket socket)
        {
            _socket = socket;
        }

        public long Handle
        {
            get
            {
                return (long)_socket.Handle;
            }
        }

        public string ReadString()
        {
            byte[] bytes = new byte[1024];

            string result = string.Empty;
            int bytesRec = _socket.Receive(bytes);
            result += Encoding.UTF8.GetString(bytes, 0, bytesRec);

            return result;
        }

        public Int64 ReadInt64()
        {
            byte[] bytes = new byte[1024];

            string result = string.Empty;
            int bytesRec = _socket.Receive(bytes);
            result += Encoding.UTF8.GetString(bytes, 0, bytesRec);

            return Int64.Parse(result);
        }

        public Int32 ReadInt32()
        {
            byte[] bytes = new byte[1024];

            string result = string.Empty;
            int bytesRec = _socket.Receive(bytes);
            result += Encoding.UTF8.GetString(bytes, 0, bytesRec);

            return Int32.Parse(result);
        }

        public void SendString(string data)
        {
            // Echo the data back to the client.
            byte[] msg = Encoding.UTF8.GetBytes(data + '\n');
            _socket.Send(msg);
        }

        public void Shutdown()
        {
            _socket.Shutdown(SocketShutdown.Both);
            _socket.Close();
        }

        public string SenderConnection
        {
            get
            {
                return ((long)_socket.Handle).ToString();
            }
        }


        public void RegisterSocket()
        {
            // Create the state object.
            StateObject state = new StateObject();
            state.workSocket = _socket;

            _socket.BeginReceive(state.buffer, 0, StateObject.BufferSize, SocketFlags.None, new AsyncCallback(ReceiveCallback), state);
        }

        private void ReceiveCallback(IAsyncResult iar)
        {
            StateObject state = (StateObject)iar.AsyncState;
            Socket client = state.workSocket;

            try
            {
                // Read data from the remote device.
                int bytesRead = client.EndReceive(iar);
                if (bytesRead > 0)
                {
                    // There might be more data, so store the data received so far.
                    state.sb.Append(Encoding.UTF8.GetString(state.buffer, 0, bytesRead));

                    string strMessage = state.sb.ToString();

                    foreach (var message in strMessage.Split('~').ToList())
                    {
                        if (message == string.Empty)
                            return;

                        if (MessageReceived != null)
                        {
                            GameSocketEventArgs socketArgs = new GameSocketEventArgs()
                            {
                                Message = message
                            };
                            MessageReceived(this, socketArgs);
                        }
                    }
                }
                else
                {
                    // Client trying to disconnect
                    ClientDisconnected(this, new EventArgs());
                }
            }
            catch(SocketException socketEx)
            {
                switch (socketEx.ErrorCode)
                {
                    case 10053: // Connection aborted
                        ClientDisconnected(this, new EventArgs());
                        break;
                    case 10054: // Client disconnected
                        ClientDisconnected(this, new EventArgs());
                        break;
                    default: // Something else
                        ClientDisconnected(this, new EventArgs());
                        break;
                }
            }
        }

        private class StateObject
        {
            // Client socket.
            public Socket workSocket = null;
            // Size of receive buffer.
            public const int BufferSize = 256;
            // Receive buffer.
            public byte[] buffer = new byte[BufferSize];
            // Received data string.
            public StringBuilder sb = new StringBuilder();
        }
    }
}
