﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameServer.Networking
{
    class GameWebSocket : ISocketConnection
    {
        private string _id;
        private WebSocketMessage _interface;

        public string SenderConnection
        {
            get
            {
                return _id;
            }
        }

        public GameWebSocket(string id, WebSocketMessage socketInterface)

        {
            _id = id;
            _interface = socketInterface;
        }

        public void Shutdown()
        {
        }

        public void RegisterSocket()
        { }

        public void SendString(string data)
        {
            _interface.SendString(_id, data);
        }
    }
}
