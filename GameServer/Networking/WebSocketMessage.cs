﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebSocketSharp;
using WebSocketSharp.Server;

namespace GameServer.Networking
{
    public class WebSocketMessage : WebSocketBehavior
    {
        private GameServer _messageServer;

        public WebSocketMessage()
        {
            _messageServer = GameServer.Game;
        }

        protected override void OnMessage(MessageEventArgs e)
        {
            var strMessage = Encoding.UTF8.GetString(e.RawData);
            var webSocket = new GameWebSocket(ID, this);

            foreach (var message in strMessage.Split('~').ToList())
            {
                if (message == string.Empty)
                    continue;

                _messageServer.WebSocketMessageReceived(webSocket, new GameSocket.GameSocketEventArgs() { Message = message });
            }
        }



        public void SendString(string id, string message)
        {
            Sessions.SendTo(Encoding.UTF8.GetBytes(message), id);
        }

        public void Shutdown()
        {
            OnClose(null);
        }
    }
}
