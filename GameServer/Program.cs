﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Threading;
using System.IO;


namespace GameServer
{
    public class Program
    {
        static int socketPort = 8690; // Game runs on this port
        static int webSocketPort = 8960; // Web Socket server runs on this port

        static void Main(string[] args)
        {
            // This is required for Unity WebClients to connect to our server
            // 843 is unity default socket policy server
            SocketPolicyServer sockpol = new SocketPolicyServer(SocketPolicyServer.AllPolicy, 843);
            sockpol.Start();

            GameServer g = new GameServer(socketPort, webSocketPort);
            Thread c = new Thread(new ThreadStart(g.ConnectionListener));
            c.Start();

            Thread u = new Thread(new ThreadStart(g.UpdateLoop));
            u.Start();
            
            bool serverRunning = true;
            while (serverRunning)
            {
                // This is where we should handle requests for spawning new servers and server list requests.
                Thread.Sleep(50);

                // Make it possible to send input.
                Stream inputStream = Console.OpenStandardInput();
                byte[] bytes = new byte[100];
                int outputLength = inputStream.Read(bytes, 0, 100);
                char[] chars = Encoding.UTF8.GetChars(bytes, 0, outputLength);
                string input = new string(chars);
                input = input.Replace("\r", "");
                input = input.Replace("\n", "");

                switch (input)
                {
                    case "help":
                        Console.WriteLine("Y U NEED HELP?!");
                        break;
                    case "reload":
                        // use g.function to reload here!
                        break;
                    case "q":
                        serverRunning = false;
                        break;
                    default:
                        Console.WriteLine("Unknown command: " + input);
                        break;
                }
            }
        }

        
    }
}
