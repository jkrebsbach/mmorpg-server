﻿using GameServer.Networking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using GameServer.Entities;
using Mindscape.Raygun4Net;
using WebSocketSharp.Server;

namespace GameServer
{
    public class GameServer
    {
        private int _socketPort;
        private int _webSocketPort;

        public static GameServer Game { get; set; }

        private WebSocketServiceHost _wssh;

        // Use this to translate client unique endpoint to a GameObjectID
        private Dictionary<string, Guid> _players = new Dictionary<string, Guid>();
        private int _maxPlayers = 20;

        // Database instance
        private SkyFish _skyFish;

        // All Game Objects - Could use a dictionary to store multiple levels simultaneously
        private LevelHandler _gameLevel;
        
        // One queue to handle player actions
        private Dictionary<Guid, List<int>> _inputQueue = new Dictionary<Guid, List<int>>();

        //private static RaygunClient _raygunClient = new RaygunClient("pxUYnGA1uSvC/DpnaRQZvg==");
        public static bool Running = true;

        public delegate void LoginPlayerDelegate(string playerID, int characterID, int gold, int experience, int level, ISocketConnection senderConnection);

        public GameServer(int socketPort, int webSocketPort)
        {
            Game = this;

            _socketPort = socketPort;
            _webSocketPort = webSocketPort;

            _gameLevel = new LevelHandler(this);
            _skyFish = new SkyFish();
        }

        public void UpdateLoop()
        {
            var playerLocationCheck = DateTime.UtcNow;
            var battleCountCheck = DateTime.UtcNow;
            var rnd = new Random();

            try
            {
                
                while (Running)
                {
                    Thread.Sleep(50);

                    if (battleCountCheck < DateTime.UtcNow)
                    {
                        _gameLevel.CheckBattleCounts(this);
                        battleCountCheck = DateTime.UtcNow.AddSeconds(30);
                    }

                    foreach (var battle in _gameLevel.GameBattles.Values)
                    {
                        battle.EnemyActions(rnd);

                        if (battle.GameObjects.Values.Any(go => go.Connection != null))
                        {
                            ThreadPool.QueueUserWorkItem(o => UpdatePlayers(battle, false));
                        }
                    }

                    List<BattleRewardPacket> rewards = new List<BattleRewardPacket>();
                    var finishedBattle = _gameLevel.GameBattles.Values.FirstOrDefault(battle => !battle.GameObjects.Values.Any(go => !go.Ally));
                    if (finishedBattle != null)
                    {
                        // All enemies gone
                        rewards.AddRange(finishedBattle.FinishBattle(rnd));
                        _gameLevel.GameBattles.Remove(finishedBattle.BattleID);

                        ThreadPool.QueueUserWorkItem(o => DestroyBattle(finishedBattle));
                    }

                    rewards.ForEach(r => SendBattleReward(r));

                    if (playerLocationCheck < DateTime.UtcNow)
                    {
                        foreach (var battle in _gameLevel.GameBattles.Values)
                        {
                            battle.UpdateBattleWanderDirection(rnd, _gameLevel.GameObjects);
                        }

                        PlayerLocationCheck();
                        playerLocationCheck = DateTime.UtcNow.AddSeconds(1);
                    }
                }
            }
            catch (Exception ex)
            {
                //_raygunClient.Send(ex);
                Thread.Sleep(50);
                throw;
            }
        }

        private void PlayerLocationCheck()
        {
            lock (this)
            {
                foreach (var gameObject in _gameLevel.GameObjects)
                {
                    var player = gameObject as Player;

                    if (player == null)
                        continue;

                    if (player.CurrentBattle == null)
                    {
                        player.CheckBuffs();

                        // Show nearby players
                        foreach (var nearbyPlayer in _gameLevel.GameObjects.Where(go => go.CurrentBattle == null && go.CurrentLocation == player.CurrentLocation && go.Scene == player.Scene &&
                            go.GameObjectID != player.GameObjectID && go.MapPosition.Overlap(player.MapPosition, 6)))
                        {
                            SendPlayerPosition(nearbyPlayer, player.Connection);
                        }

                        // Show nearby Battles
                        foreach (var nearbyBattle in _gameLevel.GameBattles.Values.Where(gb =>
                            ((gb.LocationID == 0 && player.CurrentLocation == null) || (player.CurrentLocation != null && gb.LocationID == player.CurrentLocation.LocationID)) &&
                                gb.Scene == player.Scene && gb.OverworldPosition.Overlap(player.MapPosition, 6)))
                        {
                            SendUpdateBattleHandler(nearbyBattle, player.Connection);
                        }

                        // Show nearby NPCs
                        if (player.CurrentLocation != null)
                        {
                            foreach (var npc in player.CurrentLocation.NPCs)
                            {
                                SendNPCPosition(npc, player.Connection);
                            }
                        }

                        if (player.UpdateInventoryNeeded)
                        {
                            SendInventory(player.Connection, player);
                        }
                        if (player.CheckPositionUpdateTimer())
                        {
                            _skyFish.UpdateCharacterPosition(player);
                        }
                        if (player.Dead && player.CheckRespawnTimer())
                        {
                            player.Resurect();
                            SendRespawn(player.Connection);
                        }
                    }
                }
            }
        }

        public void ConnectionListener()
        {
            var wss = new WebSocketServer(IPAddress.Any, _webSocketPort, false);
            wss.AddWebSocketService<WebSocketMessage>("/");

            wss.Start();

            _wssh = wss.WebSocketServices["/"];

            // Data buffer for incoming data.
            byte[] bytes = new Byte[1024];

            try
            {
                IPHostEntry ipHost = Dns.GetHostEntry(Dns.GetHostName());
                //IPAddress ipAddress = ipHost.AddressList[0];
                
                //IPAddress ipAddress = IPAddress.Parse("192.168.0.22");
                //IPAddress ipAddress = IPAddress.Parse("10.102.170.203");
                IPEndPoint localEndPoint = new IPEndPoint(IPAddress.Any, _socketPort);
                Socket listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            
                listener.Bind(localEndPoint);
                listener.Listen(100);

                // Start listening for connections.
                while (Running)
                {
                    Console.WriteLine("Waiting for a connection...");
                    // Program is suspended while waiting for an incoming connection.
                    // Move this into seperate thread
                    GameSocket handler = new GameSocket(listener.Accept());

                    // New connection
                    string connect = handler.ReadString();

                    handler.MessageReceived += OnMessageReceived;
                    handler.ClientDisconnected += OnClientDisconnected;

                    LoginRequest(connect, handler);


                }
            }
            catch (Exception ex)
            {
                //_raygunClient.Send(ex);
                LogWriter.Log.WriteMessage(ex.Message);
                throw;
            }
        }

        private void OnMessageReceived(Object sender, GameSocket.GameSocketEventArgs e)
        {
            GameSocket socket = (GameSocket)sender;

            var dataUnit = FindUnit(socket);
            HandleMessage(socket, dataUnit, e.Message);

            // Prepare for next message
            socket.RegisterSocket();
        }

        internal void WebSocketMessageReceived(GameWebSocket webSocket, GameSocket.GameSocketEventArgs e)
        {
            var dataUnit = FindUnit(webSocket);
            if (dataUnit == null)
            {
                LoginRequest(e.Message, webSocket);
            }
            else
            {
                HandleMessage(webSocket, dataUnit, e.Message);
            }
        }

        private void LoginRequest(string connect, ISocketConnection handler)
        { 
            string[] pieces = connect.Replace("~", "").Split('|');
                    
                    if (pieces.Length< 2)
                    {
                        LogWriter.Log.WriteMessage("incomplete login received: " + connect);
                    }
                    else if (_players.Count >= _maxPlayers) // Too many players
                    {
                        LogWriter.Log.WriteMessage("Too many players");

                        NetMessage sendGameObject = new NetMessage();
    sendGameObject.Write((long)NetworkPackageToClient.ShutDown);
                        sendGameObject.Write("Too many players");
            
                        try
                        {
                            sendGameObject.SendMessage(handler, NetDeliveryMethod.ReliableOrdered);
                        }
                        catch
                        {
                            // Sometimes this errors out?...
                        }

                        handler.Shutdown();
                    }
                    else
                    {
                        string email = pieces[0];
                        string strToken = pieces[1];
                        if (strToken.Length > 36)
                            strToken = strToken.Substring(0, 36);
                        Guid token = new Guid(strToken);

                        var authentication = _skyFish.CheckLogin(email, token);
                        
                        if (authentication.Authenticated)
                        {
                            var delLP = new LoginPlayerDelegate(LoginPlayer);
                            delLP.BeginInvoke(authentication.UserID, authentication.CharacterID, 
                                authentication.Gold, authentication.Experience, authentication.Level,
                                handler, null, null);
                        }
                        else
                        {
                            NetMessage sendGameObject = new NetMessage();
                            sendGameObject.Write((long)NetworkPackageToClient.ShutDown);
                            sendGameObject.Write("Login failure");

                            try
                            {
                                sendGameObject.SendMessage(handler, NetDeliveryMethod.ReliableOrdered);
                            }
                            catch
                            {
                                // Sometimes this errors out?...
                            }

                            handler.Shutdown();
                        }
                    }
}

        private void OnClientDisconnected(Object sender, EventArgs e)
        {
            lock (this)
            {
                GameSocket socket = (GameSocket)sender;
                GameObject unit = FindUnit(socket);

                if (unit != null)
                {
                    _gameLevel.GameObjects.Remove(unit);
                    LogWriter.Log.DebugMessage("NetConnectionStatus", "Player has disconnected: [name] (" + unit.GameObjectID + ")");

                    // Remove client from any battle handlers
                    if (unit.CurrentBattle != null)
                    {
                        BattleHandler currentBattle = unit.CurrentBattle;
                        currentBattle.LeaveBattle(unit);

                        // Notify remaining players that character has left
                        foreach (GameObject go in currentBattle.GameObjects.Values.Where(player => player.Connection != null))
                        {
                            DisposeGameObject(go.Connection, unit);
                        }
                    }

                }

                
                if (socket != null)
                {
                    if (_players.ContainsKey(socket.SenderConnection))
                    {
                        _players.Remove(socket.SenderConnection);
                    }

                    if (unit != null)
                        DisposeGameObject(socket, unit);
                }
            }
        }

        private void LoginPlayer(string userID, int characterID, int gold, int experience, int level, ISocketConnection senderConnection)
        {
            var character = _skyFish.RetrieveCharacter(characterID);

            // Assume resource name of 'player', health of 1000, energy of 50
            Player unit = GameObject.InitializePlayer(senderConnection, character, userID);

            if (unit != null)
            {
                unit.CharacterLevel = level;
                unit.SkillPoints = character.SkillPoints;
                unit.AddGold(gold);
                unit.AddExperience(experience);
                unit.PriorQuests = character.PriorQuests;
                unit.QuestKillCounts = character.QuestKillCounts;
                unit.QuestTalkCounts = character.QuestTalkCounts;

                if (character.LocationID.HasValue)
                {
                    unit.EnterLocation(_gameLevel.Locations[character.LocationID.Value]);
                }

                _gameLevel.GameObjects.Add(unit);
                SetupInputQueue(unit.GameObjectID);

                _players[unit.Connection.SenderConnection] = unit.GameObjectID;
                senderConnection.RegisterSocket();
                LogWriter.Log.DebugMessage("NetConnectionStatus", "Player has connected: [name] (" + unit.GameObjectID + ")");
            }
            else
            {
                // Issues trying to connect player
                // Send message to client
            }
        }

        private void SetupInputQueue(Guid gameObjectID)
        {
            _inputQueue.Add(gameObjectID, new List<int>());
        }

        private void DisposeInputQueue(Guid gameObjectID)
        {
            _inputQueue.Remove(gameObjectID);
        }

        private Player FindUnit(ISocketConnection msg)
        {
            Player unit = null;
            Guid objectID = Guid.Empty;

            if (_players.ContainsKey(msg.SenderConnection))
                objectID = _players[msg.SenderConnection];

            if (objectID != Guid.Empty)
            {
                unit = _gameLevel.GameObjects.FirstOrDefault(pl => pl.GameObjectID == objectID) as Player;
            }
            else
            {
                LogWriter.Log.LogError(1, "ERROR", "Could not find user relating to message");
            }

            return unit;
        }

        private GameObject FindUnit(Guid guid)
        {
            GameObject unit = null;

            if (_gameLevel.GameObjects.Any(go => go.GameObjectID == guid))
            {
                unit = _gameLevel.GameObjects.First(go => go.GameObjectID == guid);
            }
            else
            {
                LogWriter.Log.LogError(1, "ERROR", "Could not find user relating to message");
            }

            return unit;
        }

        /// <summary>
        /// When no enemies left in a battle, destroy
        /// </summary>
        /// <param name="battle"></param>
        private void DestroyBattle(BattleHandler battle)
        {
            foreach (var go in battle.GameObjects.Values)
            {
                var player = go as Player;
                if (player != null)
                {
                    _skyFish.UpdateCharacter(player);
                }
            }

            foreach (var player in _gameLevel.GameObjects.Where(go => go.CurrentBattle == null))
            {
                // Show nearby Battles
                if (((battle.LocationID == 0 && player.CurrentLocation == null) || battle.LocationID == player.CurrentLocation.LocationID) && 
                    battle.Scene == player.Scene &&
                    battle.OverworldPosition.Overlap(player.MapPosition, 6))
                {
                    SendDestroyBattleHandler(battle, player.Connection);
                }
            }
        }

        /// <summary>
        /// When members of a battle change, notify all players in that battle of change
        /// </summary>
        /// <param name="battle"></param>
        /// <param name="forceUpdate"></param>
        private void UpdatePlayers(BattleHandler battle, bool forceUpdate)
        {
            int index = 0;
            lock (this)
            {
                foreach (GameObject gameObject in battle.GameObjects.Values)
                {
                    gameObject.CheckBuffs();

                    if (forceUpdate || gameObject.UpdateNeeded)
                    {
                        foreach (GameObject playerObject in battle.GameObjects.Values.Where(all => all.Connection != null))
                        {
                            if (gameObject.Disposed)
                            {
                                DisposeGameObject(playerObject.Connection, gameObject);
                                Enemy enemy = gameObject as Enemy;
                                Player player = playerObject as Player;
                                if (enemy != null)
                                    player.CheckQuestKillReward(enemy.EnemyID, _skyFish);
                            }
                            else
                            {
                                // Update battle character
                                NotifyGameObject(playerObject.Connection, index, gameObject);
                            }
                        }

                        gameObject.MarkUpdated();
                    }
                    if (gameObject.UpdateInventoryNeeded)
                    {
                        Player player = gameObject as Player;
                        if (player != null)
                            SendInventory(gameObject.Connection, player);
                    }
                    index++;
                }
                while (battle.GameObjects.Values.Any(go => go.Disposed))
                {
                    GameObject disposedGameObject = battle.GameObjects.Values.First(enemy => enemy.Disposed);
                    battle.LeaveBattle(disposedGameObject);
                }
            }
        }

        private void HandleMessage(ISocketConnection conn, Player unit, string message)
        {
            if (unit == null)
                return;

            string[] pieces = message.Split('|');

            int packageID = 0;
            if (!int.TryParse(pieces[0], out packageID))
            {
                LogWriter.Log.WriteMessage("incomplete message received: " + message);
                return;
            }

            NetworkPackageFromClient packId = (NetworkPackageFromClient)Enum.Parse(typeof(NetworkPackageFromClient), pieces[0]);

            var ability = 0;
            Guid target;
            GameObject targetObject = null;

            switch (packId)
            {
                case NetworkPackageFromClient.AttackInitiate:
                    target = new Guid(pieces[1]);

                    targetObject = unit.CurrentBattle.FindTarget(target);
                    if (targetObject != null)
                        unit.UseAttack(targetObject);

                    break;
                case NetworkPackageFromClient.SpellInitiate:
                    ability = int.Parse(pieces[1]);
                    target = new Guid(pieces[2]);

                    targetObject = unit.CurrentBattle.FindTarget(target);
                    if (targetObject != null)
                        unit.UseAbility(ability, targetObject);

                    break;
                case NetworkPackageFromClient.InventoryInitiate:
                    var itemID = int.Parse(pieces[1]);
                    target = new Guid(pieces[2]);

                    targetObject = unit.CurrentBattle.FindTarget(target);
                    if (targetObject != null)
                        unit.UseInventory(itemID, targetObject);

                    break;
                case NetworkPackageFromClient.AttemptDefend:
                    unit.Defend();
                    break;
                case NetworkPackageFromClient.AttemptEscape:
                    var success = unit.AttemptEscape();

                    SendEscapeAttempt(success, conn);
                    if (success)
                    {
                        LeaveBattle(unit.CurrentBattle.BattleID, unit);
                    }
                    break;
                case NetworkPackageFromClient.SwitchRow:
                    unit.SwitchRow();
                    break;
                case NetworkPackageFromClient.EnterLocation:
                    var strLocationId = pieces[1];
                    var locationId = 0;

                    if (Int32.TryParse(strLocationId, out locationId) && _gameLevel.Locations.ContainsKey(locationId))
                    {
                        Location location = _gameLevel.Locations[locationId];
                        unit.EnterLocation(location);
                        _skyFish.UpdateCharacterPosition(unit);
                    }
                    break;
                case NetworkPackageFromClient.EnterBattle:
                    var battleId = pieces[1];

                    var battleHandler = _gameLevel.GameBattles[new Guid(battleId)];
                    battleHandler.JoinBattle(unit);
                    UpdatePlayers(battleHandler, true);
                    break;
                case NetworkPackageFromClient.Respawn:
                    unit.Respawn();
                    break;
                case NetworkPackageFromClient.LeaveBattle:
                    var leavingBattleId = new Guid(pieces[1]);

                    LeaveBattle(leavingBattleId, unit);
                    break;
                case NetworkPackageFromClient.LeaveLocation:
                    unit.LeaveLocation();

                    SendLocations(unit.Connection); // Resent overworld locations
                    break;
                case NetworkPackageFromClient.LoadLocations:
                    foreach (var currentBattle in _gameLevel.GameBattles.Values)
                    {
                        SendUpdateBattleHandler(currentBattle, conn);
                    }
                    break;
                case NetworkPackageFromClient.ClientChat:
                    var chatMessage = pieces[1];
                    foreach(var player in _players.Values)
                    {
                        var recipient = _gameLevel.GameObjects.FirstOrDefault(pl => pl.GameObjectID == player);

                        SendChatMessage( unit.CharacterName, chatMessage, recipient.Connection);
                    }
                    break;
                case NetworkPackageFromClient.WhoAmI:
                    SendWhoAmI(conn, unit);
                    SendSpells(conn, unit);
                    SendInventory(conn, unit);
                    SendLocations(conn);
                    break;
                case NetworkPackageFromClient.WhoIsPlayer:
                    var playerId = new Guid(pieces[1]);
                    var targetPlayer = _gameLevel.GameObjects.FirstOrDefault(go => go.GameObjectID == playerId);

                    if (targetPlayer != null)
                    {
                        SendWhoIsPlayer(conn, targetPlayer);
                    }
                    break;
                case NetworkPackageFromClient.CharacterMovement:
                    if (pieces.Length < 4 || pieces[3] == string.Empty)
                    {
                        LogWriter.Log.WriteMessage("incomplete message received: " + message);
                        return;
                    }

                    // Character moving should not be in a battle...
                    unit.LeaveBattle();

                    var movementX = float.Parse(pieces[1]);
                    var movementY = float.Parse(pieces[2]);
                    var scene = int.Parse(pieces[3]);
                    unit.UpdatePosition(new Math.Vector2(movementX, movementY), scene);
                    break;
                case NetworkPackageFromClient.UpdateEquipment:
                    if (pieces.Length < 6)
                        return;

                    var leftHand = int.Parse(pieces[1]);
                    var rightHand = int.Parse(pieces[2]);
                    var head = int.Parse(pieces[3]);
                    var body = int.Parse(pieces[4]);
                    var ranged = int.Parse(pieces[5]);

                    unit.EquipLeftHand = leftHand;
                    unit.EquipRightHand = rightHand;
                    unit.EquipHead = head;
                    unit.EquipBody = body;
                    unit.EquipRanged = ranged;

                    _skyFish.UpdateCharacter(unit);
                    break;
                case NetworkPackageFromClient.StartDialog:
                    var dialogNpcId = int.Parse(pieces[1]);

                    var questId = unit.CheckQuestTalkReward(dialogNpcId, _skyFish);
                    SendStartDialog(questId, dialogNpcId, conn);
                    break;
                case NetworkPackageFromClient.StoreInventory:
                    var storeId = int.Parse(pieces[1]);
                    var storeNpcId = int.Parse(pieces[2]);

                    var storeInventory = _skyFish.GetStoreInventory(storeId, storeNpcId);
                    SendStoreInventory(storeInventory, conn);
                    break;
                case NetworkPackageFromClient.QuestSelection:
                    var questNpcId = int.Parse(pieces[1]);

                    List<Quest> quests = null;
                    if (questNpcId == -1)
                        quests = _skyFish.GetCharacterQuests(unit.CharacterID, _gameLevel.QuestLibrary);
                    else
                        quests = _skyFish.GetQuests(questNpcId, unit.CharacterID, _gameLevel.QuestLibrary);

                    quests.ForEach(q => q.CheckConditions(unit));

                    SendQuestSelection(quests.Where(q => q.AvailabilityConditionsMet || q.Status == Quest.QuestStatusEnum.Started), conn);
                    break;
                case NetworkPackageFromClient.StartQuest:
                    var startQuestNpcId = int.Parse(pieces[1]);
                    var startQuestId = int.Parse(pieces[2]);

                    var questNpc = unit.CurrentLocation.NPCs.FirstOrDefault(n => n.NPCID == startQuestNpcId);
                    if (questNpc != null && unit.MapPosition.Overlap(questNpc.MapPosition, 1.5f))
                    {
                        var startQuest = _skyFish.GetQuests(startQuestNpcId, unit.CharacterID, _gameLevel.QuestLibrary).FirstOrDefault(q => q.QuestID == startQuestId);

                        if (startQuest != null)
                        {
                            unit.InitiateQuest(startQuest);
                            _skyFish.StartQuest(startQuest, unit);
                        }
                    }
                    break;
                case NetworkPackageFromClient.PurchaseItem:
                    var purchaseItemNpcId = int.Parse(pieces[1]);
                    var purchaseItemStoreId = int.Parse(pieces[2]);
                    var purchaseItemId = int.Parse(pieces[3]);

                    var itemNpc = unit.CurrentLocation.NPCs.FirstOrDefault(n => n.NPCID == purchaseItemNpcId);
                    var purchaseStoreInventory = _skyFish.GetStoreInventory(purchaseItemStoreId, purchaseItemNpcId);
                    var purchaseItem = purchaseStoreInventory.FirstOrDefault(si => si.ItemID == purchaseItemId);
                    if (itemNpc != null && purchaseItem != null)
                    {
                        purchaseItem.Cost = (int)((decimal)purchaseItem.Cost * (1.0M - (unit.Abilities.Sum(a => a.Value.ShopCostDecrease)/100M)));

                        if (unit.Gold > purchaseItem.Cost && unit.MapPosition.Overlap(itemNpc.MapPosition, 1.5f))
                        {
                            var item = LevelHandler.InventoryTemplate(purchaseItemId, 1);
                            var quantity = unit.AddInventory(item);
                            _skyFish.PurchaseItem(purchaseItemId, unit.CharacterID, purchaseItem.Cost, quantity);
                        }
                    }
                    break;
                case NetworkPackageFromClient.SellItem:
                    var sellItemNpcId = int.Parse(pieces[1]);
                    var sellItemStoreId = int.Parse(pieces[2]);
                    var sellItemId = int.Parse(pieces[3]);

                    var sellItemNpc = unit.CurrentLocation.NPCs.FirstOrDefault(n => n.NPCID == sellItemNpcId);
                    var sellItemQty = unit.InventoryItemQty(sellItemId);
                    var sellItemCost = unit.InventoryItemCost(sellItemId);
                    if (sellItemNpc != null && sellItemQty > 0 && unit.MapPosition.Overlap(sellItemNpc.MapPosition, 1.5f))
                    {
                        sellItemCost = (int)((decimal)sellItemCost * (1.0M + (unit.Abilities.Sum(a => a.Value.ShopCostDecrease) / 100M)));

                        unit.ReduceInventory(sellItemId);
                        _skyFish.SellItem(sellItemId, unit.CharacterID, sellItemCost, 1);
                    }
                    break;
                case NetworkPackageFromClient.CompleteQuest:
                    var completeQuestNpcId = int.Parse(pieces[1]);
                    var completeQuestId = int.Parse(pieces[2]);

                    var completeQuestNpc = unit.CurrentLocation.NPCs.FirstOrDefault(n => n.NPCID == completeQuestNpcId);
                    if (completeQuestNpc != null && unit.MapPosition.Overlap(completeQuestNpc.MapPosition, 1.5f))
                    {
                        var completeQuest = _skyFish.GetQuests(completeQuestNpcId, unit.CharacterID, _gameLevel.QuestLibrary).FirstOrDefault(q => q.QuestID == completeQuestId);

                        if (completeQuest != null)
                        {
                            _skyFish.CompleteQuest(completeQuest, unit);
                            unit.CompleteQuest(completeQuest);
                            
                            completeQuest.ApplyRewards(unit);
                        }
                    }                    
                    break;
                case NetworkPackageFromClient.LearnSkill:
                    var abilityId = int.Parse(pieces[1]);

                    if (unit.LearnAbility(abilityId))
                    {
                        _skyFish.UpdateCharacter(unit);
                        _skyFish.UpdateCharacterAbilities(unit);
                        SendSkillLearned(abilityId, unit.Connection);
                    }
                    else
                    {
                        SendSkillLearned(-1, unit.Connection);
                    }
                    break;
                case NetworkPackageFromClient.Logout:
                    // Character exiting - save character position for when they return
                    _skyFish.UpdateCharacterPosition(unit);
                    break;
                default:

                    LogWriter.Log.WriteMessage("Unknown package (" + (long)packId + ") arived from [" + unit.GameObjectID.ToString() + "] . Package Dumped.");
                    break;
            }
        }

        private void LeaveBattle(Guid leavingBattleId, GameObject unit)
        {
            var leavingBattleHandler = _gameLevel.GameBattles[leavingBattleId];
            leavingBattleHandler.LeaveBattle(unit);

            // Notify remaining players that character has left
            foreach (var go in leavingBattleHandler.GameObjects.Values.Where(player => player.Connection != null))
            {
                DisposeGameObject(go.Connection, unit);
            }
        }

        public static void SendWhoIsPlayer(ISocketConnection connection, GameObject player)
        {
            NetMessage sendGameObject = new NetMessage();
            sendGameObject.Write((long)NetworkPackageToClient.WhoIsPlayer);
            sendGameObject.Write(player.GameObjectID);
            sendGameObject.Write(player.TextureName);
            sendGameObject.Write(player.CharacterName);

            try
            {
                sendGameObject.SendMessage(connection, NetDeliveryMethod.ReliableOrdered);
            }
            catch
            {
                // Sometimes this errors out?...
            }
        }

        public static void SendStartDialog(int questId, int npcId, ISocketConnection connection)
        {
            NetMessage sendGameObject = new NetMessage();
            sendGameObject.Write((long)NetworkPackageToClient.DialogSelection);
            sendGameObject.Write(questId);
            sendGameObject.Write(npcId);

            try
            {
                sendGameObject.SendMessage(connection, NetDeliveryMethod.ReliableOrdered);
            }
            catch
            {
                // Sometimes this errors out?...
            }
        }

        public static void SendStoreInventory(List<StoreInventory> storeInventory, ISocketConnection connection)
        {
            NetMessage sendGameObject = new NetMessage();
            sendGameObject.Write((long)NetworkPackageToClient.StoreInventory);
            foreach (var inventory in storeInventory)
                sendGameObject.Write(string.Format("{0}~{1}", inventory.ItemID, inventory.Cost));

            try
            {
                sendGameObject.SendMessage(connection, NetDeliveryMethod.ReliableOrdered);
            }
            catch
            {
                // Sometimes this errors out?...
            }
        }

        public static void SendSkillLearned(int abilityId, ISocketConnection connection)
        {
            NetMessage sendGameObject = new NetMessage();
            sendGameObject.Write((long)NetworkPackageToClient.SkillLearned);
            sendGameObject.Write(abilityId);

            try
            {
                sendGameObject.SendMessage(connection, NetDeliveryMethod.ReliableOrdered);
            }
            catch
            {
                // Sometimes this errors out?...
            }
        }

        public static void SendQuestSelection(IEnumerable<Quest> quests, ISocketConnection connection)
        {
            NetMessage sendGameObject = new NetMessage();
            sendGameObject.Write((long)NetworkPackageToClient.QuestSelection);
            foreach (var quest in quests)
                sendGameObject.Write(string.Format("{0}~{1}~{2}~{3}", quest.QuestID, (int)quest.Status, 
                    quest.RewardConditionsMet ? 1 : 0, quest.AvailabilityConditionsMet ? 1 : 0));

            try
            {
                sendGameObject.SendMessage(connection, NetDeliveryMethod.ReliableOrdered);
            }
            catch
            {
                // Sometimes this errors out?...
            }
        }

        private static void SendPlayerPosition(GameObject nearbyPlayer, ISocketConnection conn)
        {
            NetMessage sendMovement = new NetMessage();
            sendMovement.Write((long)NetworkPackageToClient.CharacterMovement);
            sendMovement.Write(nearbyPlayer.GameObjectID.ToString());
            sendMovement.Write(nearbyPlayer.MapPosition.x.ToString());
            sendMovement.Write(nearbyPlayer.MapPosition.y.ToString());
            sendMovement.Write(nearbyPlayer.Scene);
            
            try
            {
                sendMovement.SendMessage(conn, NetDeliveryMethod.ReliableOrdered);
            }
            catch
            {
                // Sometimes this errors out?...
            }
        }

        private static void SendNPCPosition(NPC nearbyNpc, ISocketConnection conn)
        {
            var sendMovement = new NetMessage();
            sendMovement.Write((long)NetworkPackageToClient.NPCMovement);
            sendMovement.Write(nearbyNpc.NPCID.ToString());
            sendMovement.Write(nearbyNpc.Scene.ToString());
            sendMovement.Write(nearbyNpc.MapPosition.x.ToString());
            sendMovement.Write(nearbyNpc.MapPosition.y.ToString());
            try
            {
                sendMovement.SendMessage(conn, NetDeliveryMethod.ReliableOrdered);
            }
            catch
            {
                // Sometimes this errors out?...
            }
        }

        private static void SendRespawn(ISocketConnection conn)
        {
            NetMessage sendRespawn = new NetMessage();
            sendRespawn.Write((long)NetworkPackageToClient.Respawn);

            try
            {
                sendRespawn.SendMessage(conn, NetDeliveryMethod.ReliableOrdered);
            }
            catch
            {
                // Sometimes this errors out?...
            }
        }

        private static void SendEscapeAttempt(bool success, ISocketConnection conn)
        {
            NetMessage sendEscapeAttempt = new NetMessage();
            sendEscapeAttempt.Write((long)NetworkPackageToClient.EscapeAttempt);
            sendEscapeAttempt.Write(success.ToString());
            try
            {
                sendEscapeAttempt.SendMessage(conn, NetDeliveryMethod.ReliableOrdered);
            }
            catch
            {
                // Sometimes this errors out?...
            }
        }

        private static void SendWhoAmI(ISocketConnection conn, Player gameObject)
        {
            NetMessage sendWhoAmI = new NetMessage();
            sendWhoAmI.Write((long)NetworkPackageToClient.WhoAmI);
            sendWhoAmI.Write(gameObject.GameObjectID.ToString());
            sendWhoAmI.Write(gameObject.MaxHealth);
            sendWhoAmI.Write(gameObject.Health);
            sendWhoAmI.Write(gameObject.MaxEnergy);
            sendWhoAmI.Write(gameObject.Energy);
            sendWhoAmI.Write(gameObject.Gold);

            sendWhoAmI.Write(gameObject.EquipLeftHand);
            sendWhoAmI.Write(gameObject.EquipRightHand);
            sendWhoAmI.Write(gameObject.EquipBody);
            sendWhoAmI.Write(gameObject.EquipHead);

            sendWhoAmI.Write(gameObject.CharacterTypeID);
            sendWhoAmI.Write(gameObject.SkillPoints);
            sendWhoAmI.Write(gameObject.CurrentLocation == null ? 0 : gameObject.CurrentLocation.LocationID);
            sendWhoAmI.Write(gameObject.Scene);
            sendWhoAmI.Write(gameObject.MapPosition.x);
            sendWhoAmI.Write(gameObject.MapPosition.y);
            sendWhoAmI.Write(gameObject.CharacterLevel);
            sendWhoAmI.Write(gameObject.Experience);

            sendWhoAmI.Write(gameObject.EquipRanged);

            try
            {
                sendWhoAmI.SendMessage(conn, NetDeliveryMethod.ReliableOrdered);
            }
            catch
            {
                // Sometimes this errors out?...
            }
        }

        private static void SendSpells(ISocketConnection conn, GameObject gameObject)
        {
            NetMessage sendSpells = new NetMessage();
            sendSpells.Write((long)NetworkPackageToClient.UpdateSpells);

            foreach(var ability in gameObject.Abilities.Values)
            {
                sendSpells.Write(String.Format("{0}~{1}",
                    ability.AbilityID, ability.Level));
            }
            try
            {
                sendSpells.SendMessage(conn, NetDeliveryMethod.ReliableOrdered);
            }
            catch
            {
                // Sometimes this errors out?...
            }
        }

        private void SendInventory(ISocketConnection conn, Player player)
        {
            NetMessage sendInventory = new NetMessage();
            sendInventory.Write((long)NetworkPackageToClient.UpdateInventory);
            foreach (var inventoryItem in player.GetInventory())
            {
                sendInventory.Write(String.Format("{0}~{1}", inventoryItem.AbilityID, inventoryItem.Quantity));
            }
            
            try
            {
                sendInventory.SendMessage(conn, NetDeliveryMethod.ReliableOrdered);
            }
            catch
            {
                // Sometimes this errors out?...
            }

            _skyFish.UpdateCharacterInventory(player);
            player.MarkInventoryUpdated();

        }

        private void SendLocations(ISocketConnection conn)
        {
            var sendLocation = new NetMessage();
            sendLocation.Write((long)NetworkPackageToClient.UpdateLocationHandler);
            foreach (var location in _gameLevel.Locations.Values)
            {
                sendLocation.Write(String.Format("{0}~{1}~{2}~{3}~{4}~{5}", 
                    location.LocationID, location.OverworldPosition.x, location.OverworldPosition.y, location.LocationResource,
                    location.PlayerStart.x, location.PlayerStart.y));
            }

            try
            {
                sendLocation.SendMessage(conn, NetDeliveryMethod.ReliableOrdered);
            }
            catch
            {
                // Sometimes this errors out?...
            }
        }

        private static void NotifyGameObject(ISocketConnection connection, int index, GameObject unit)
        {
            var sendGameObject = new NetMessage();
            sendGameObject.Write((long)NetworkPackageToClient.UpdateBattleEntity);
            sendGameObject.Write(unit.GameObjectID.ToString());
            sendGameObject.Write(unit.Health); 
            sendGameObject.Write(unit.MaxHealth); 
            sendGameObject.Write(unit.Energy);
            sendGameObject.Write(unit.MaxEnergy);
            sendGameObject.Write(unit.TextureName); 
            sendGameObject.Write(index); 
            sendGameObject.Write(unit.Ally ? 1 : 0); 
            sendGameObject.Write(unit.CharacterName);
            sendGameObject.Write(unit.CoolDown.ToString());
            sendGameObject.Write(unit.FrontRow ? 1 : 0);
            sendGameObject.Write(unit.BuffString);

            try
            {
                sendGameObject.SendMessage(connection, NetDeliveryMethod.ReliableOrdered);
            }
            catch
            {
                // Sometimes this errors out?...
            }
        }

        private void DisposeGameObject(ISocketConnection connection, GameObject unit)
        {
            NetMessage sendGameObject = new NetMessage();
            sendGameObject.Write((long)NetworkPackageToClient.DestroyBattleEntity);
            sendGameObject.Write(unit.GameObjectID.ToString());

            try
            {
                sendGameObject.SendMessage(connection, NetDeliveryMethod.ReliableOrdered);
            }
            catch
            {
                // Sometimes this errors out?...
            }
        }

        public void SendInitiateAbility(BattleHandler battle, int abilityID, GameObject target, GameObject caster)
        {
            foreach (GameObject player in battle.GameObjects.Values.Where(all => all.Connection != null))
            {
                NetMessage sendGameObject = new NetMessage();
                sendGameObject.Write((long)NetworkPackageToClient.InitiateAbility);
                sendGameObject.Write(caster.GameObjectID.ToString());
                sendGameObject.Write(target.GameObjectID.ToString());
                sendGameObject.Write(abilityID.ToString());
                
                try
                {
                    sendGameObject.SendMessage(player.Connection, NetDeliveryMethod.ReliableOrdered);
                }
                catch
                {
                    // Sometimes this errors out?...
                }
            }
        }

        public void SendUpdateHealth(BattleHandler battle, GameObject target, int damage)
        {
            foreach (GameObject player in battle.GameObjects.Values.Where(all => all.Connection != null))
            {
                NetMessage sendGameObject = new NetMessage();
                sendGameObject.Write((long)NetworkPackageToClient.UpdateHealth);
                sendGameObject.Write(target.GameObjectID.ToString());
                sendGameObject.Write(damage);

                try
                {
                    sendGameObject.SendMessage(player.Connection, NetDeliveryMethod.ReliableOrdered);
                }
                catch
                {
                    // Sometimes this errors out?...
                }
            }
        }

        public void SendChatMessage(string characterName, string message, ISocketConnection connection)
        {
            NetMessage sendGameObject = new NetMessage();
            sendGameObject.Write((long)NetworkPackageToClient.ChatMessage);
            sendGameObject.Write(characterName);
            sendGameObject.Write(message);
            
            try
            {
                sendGameObject.SendMessage(connection, NetDeliveryMethod.ReliableOrdered);
            }
            catch
            {
                // Sometimes this errors out?...
            }
        }

        public void SendBattleReward(BattleRewardPacket reward)
        {
            string loot = string.Empty;
            string delim = string.Empty;
            foreach(var item in reward.Items)
            {
                loot += string.Format("{0}{1},{2}", delim, item.InventoryItemID, 1);
                delim = "~";
            }

            NetMessage sendRewardObject = new NetMessage();
            sendRewardObject.Write((long)NetworkPackageToClient.BattleReward);
            sendRewardObject.Write(reward.Experience.ToString());
            sendRewardObject.Write(reward.Gold.ToString());
            sendRewardObject.Write(loot);
            sendRewardObject.Write(reward.LevelUp ? 1 : 0);
            
            try
            {
                sendRewardObject.SendMessage(reward.Player.Connection, NetDeliveryMethod.ReliableOrdered);
            }
            catch
            {
                // Sometimes this errors out?...
            }
        }

        public void SendDestroyBattleHandler(BattleHandler battleHandler, ISocketConnection connection)
        {
            NetMessage sendGameObject = new NetMessage();
            sendGameObject.Write((long)NetworkPackageToClient.DestroyBattleHandler);
            sendGameObject.Write(battleHandler.BattleID.ToString());

            try
            {
                sendGameObject.SendMessage(connection, NetDeliveryMethod.ReliableOrdered);
            }
            catch
            {
                // Sometimes this errors out?...
            }
        }

        public void SendUpdateBattleHandler(BattleHandler battleHandler, ISocketConnection connection)
        {
            string characterTypes = String.Join("~", battleHandler.GameObjects.Values.Where(go => go.Ally).Select(go => ((Player)go).CharacterTypeID));

            NetMessage sendGameObject = new NetMessage();
            sendGameObject.Write((long)NetworkPackageToClient.UpdateBattleHandler);
            sendGameObject.Write(battleHandler.BattleID.ToString());
            sendGameObject.Write(battleHandler.OverworldPosition.x);
            sendGameObject.Write(battleHandler.OverworldPosition.y);
            sendGameObject.Write(battleHandler.OverworldWander.x);
            sendGameObject.Write(battleHandler.OverworldWander.y);
            sendGameObject.Write(characterTypes);
            sendGameObject.Write(battleHandler.Scene);
            sendGameObject.Write(battleHandler.Resource);

            try
            {
                sendGameObject.SendMessage(connection, NetDeliveryMethod.ReliableOrdered);
            }
            catch
            {
                // Sometimes this errors out?...
            }
        }
    }
}
