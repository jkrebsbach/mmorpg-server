using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Diagnostics;
using Microsoft.WindowsAzure.ServiceRuntime;
using Microsoft.WindowsAzure.Storage;

namespace RobotAvengerSvc
{
    public class WorkerRole : RoleEntryPoint
    {
        public override void Run()
        {
            // This is a sample worker implementation. Replace with your logic.
            Trace.TraceInformation("RobotAvengerSvc entry point called");

            var serverEndpoint = RoleEnvironment.CurrentRoleInstance.InstanceEndpoints["ServerEndpoint"].IPEndpoint;
            var webSocketEndpoint = RoleEnvironment.CurrentRoleInstance.InstanceEndpoints["WebSocketEndpoint"].IPEndpoint;

            // This is required for Unity WebClients to connect to our server
            // 843 is unity default socket policy server
            SocketPolicyServer sockpol = new SocketPolicyServer(SocketPolicyServer.AllPolicy,
                RoleEnvironment.CurrentRoleInstance.InstanceEndpoints["SocketPolicyServer"].IPEndpoint.Port);
            sockpol.Start();

            GameServer.GameServer g = new GameServer.GameServer(serverEndpoint.Port, webSocketEndpoint.Port);
            var c = new Thread(g.ConnectionListener);
            c.Start();

            var u = new Thread(g.UpdateLoop);
            u.Start();

            while (true)
            {
                Thread.Sleep(10000);
                Trace.TraceInformation("Working");
            }
        }

        public override bool OnStart()
        {
            // Set the maximum number of concurrent connections 
            ServicePointManager.DefaultConnectionLimit = 12;

            // For information on handling configuration changes
            // see the MSDN topic at http://go.microsoft.com/fwlink/?LinkId=166357.

            return base.OnStart();
        }

        public override void OnStop()
        {
            GameServer.GameServer.Running = false;
        }
    }
}
