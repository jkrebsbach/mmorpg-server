﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GameServer.Entities;
using GameServer;
using System.Collections.Generic;

namespace GameTestProject
{
    [TestClass]
    public class CombatTests
    {
        [TestMethod]
        public void TestMarkTarget()
        {
            Dictionary<int, Ability> abilities = new Dictionary<int, Ability>();

            abilities[2] = MarkTarget1();

            var enemy = MakeEnemy();
            var player = MakePlayer(abilities);

            player.UseAbility(2, enemy);
        }

        [TestMethod]
        public void TestDoubleStrike1()
        {
            Dictionary<int, Ability> abilities = new Dictionary<int, Ability>();

            abilities[1] = DoubleStrike1();

            var enemy = MakeEnemy();
            var player = MakePlayer(abilities);

            player.UseAbility(1, enemy);
        }



        #region Abilities

        private Ability DoubleStrike1()
        {
            var result = new Ability(1, "DoubleStrike", 1, 0, 0, 5, true, new List<int>() { 1 }, Ability.ElementalEnum.None, null);

            result.StrikeTwice = true;
            result.DamageType = Ability.DamageTypeEnum.Physical;
            
            return result;
        }

        private Ability DoubleStrike2()
        {
            var result = new Ability(1, "DoubleStrike", 2, 0, 0, 5, true, new List<int>() { 1 }, Ability.ElementalEnum.None, null);

            result.StrikeTwice = true;
            result.InitialHitChanceIncrease = 30;
            result.DamageType = Ability.DamageTypeEnum.Physical;

            return result;
        }

        private Ability DoubleStrike3()
        {
            var result = new Ability(1, "DoubleStrike", 3, 0, 0, 5, true, new List<int>() { 1 }, Ability.ElementalEnum.None, null);

            result.StrikeTwice = true;
            result.InitialHitChanceIncrease = 60;
            result.DamageType = Ability.DamageTypeEnum.Physical;

            return result;
        }

        private Ability MarkTarget1()
        {
            var result = new Ability(2, "MarkTarget", 1, 0, 0, 4, true, new List<int>() { 1 }, Ability.ElementalEnum.None, null);
            result.ResistTarget = "willpower";
            result.OffsetCalc = "agility";
            result.OffsetTarget = "charisma";

            result.PercentOffset = -20;
            result.DefaultDuration = 30;

            return result;
        }

        private Ability MarkTarget2()
        {
            var result = new Ability(2, "MarkTarget", 2, 0, 0, 6, true, new List<int>() { 1 }, Ability.ElementalEnum.None, null);
            result.ResistTarget = "willpower";
            result.OffsetCalc = "agility";
            result.OffsetTarget = "charisma";

            result.PercentOffset = -30;
            result.DefaultDuration = 30;

            return result;
        }

        private Ability MarkTarget3()
        {
            var result = new Ability(2, "MarkTarget", 3, 0, 0, 8, true, new List<int>() { 1 }, Ability.ElementalEnum.None, null);
            result.ResistTarget = "willpower";
            result.OffsetCalc = "agility";
            result.OffsetTarget = "charisma";

            result.PercentOffset = -40;
            result.DefaultDuration = 30;

            return result;
        }

        #endregion

        #region characters

        private GameObject MakeEnemy()
        {
            CharacterModel character = new CharacterModel()
            {
                MaxHealth = 750,
                MaxEnergy = 40,
                Defense = 7,
                Strength = 9,
                Agility = 5,
                Charisma = 6,
                Willpower = 7,
                Intelligence = 4
            };

            return new GameObject(GameObject.AICategory.None, null, "BAD_GUY", character, false, null, null);
        }

        private GameObject MakePlayer(Dictionary<int, Ability> abilities)
        {
            CharacterModel character = new CharacterModel()
            {
                MaxHealth = 750,
                MaxEnergy = 40,
                Defense = 7,
                Strength = 9,
                Agility = 5,
                Charisma = 6,
                Willpower = 7,
                Intelligence = 4
            };

            return new GameObject(GameObject.AICategory.None, null, "GOOD_GUY", character, false, abilities, null);
        }

        #endregion
    }
}
