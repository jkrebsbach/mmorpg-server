﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace GameWeb
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Android",
                url: "android",
                defaults: new { controller = "Account", action = "ThirdPartySuccess", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "iOS",
                url: "iphone",
                defaults: new { controller = "Account", action = "ThirdPartySuccess", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

        }
    }
}
