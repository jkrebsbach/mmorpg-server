﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GameWeb.Models
{
    public class CharacterModel
    {
        public Dictionary<int, string> CharacterOptions
        {
            get
            {
                Dictionary<int, string> result = new Dictionary<int, string>();
                result[1] = "Woodsman";
                result[2] = "Merchant";

                return result;
            }
        }

        public static CharacterModel InitializeCharacter(int characterTypeID)
        {
            CharacterModel result = new CharacterModel();
            result.Level = 1;
            result.SkillPoints = 1;

            result.CharacterTypeID = characterTypeID;

            if (characterTypeID == 1) // woodsman
            {
                result.MaxHealth = 750;
                result.MaxEnergy = 40;
                result.Defense = 7;
                result.Strength = 9;
                result.Agility = 5;
                result.Charisma = 6;
                result.Willpower = 7;
                result.Intelligence = 4;
            }
            else // merchant
            {
                result.MaxHealth = 650;
                result.MaxEnergy = 60;
                result.Defense = 6;
                result.Strength = 7;
                result.Agility = 5;
                result.Charisma = 8;
                result.Willpower = 6;
                result.Intelligence = 7;
            }

            // everybody gets a knife
            result.Inventory.Add(new CharacterInventoryModel()
                {
                    ItemID = 2,
                    Quantity = 1
                });

            return result;
        }

        public int CharacterID { get; set; }
        public int MaxHealth { get; set; }
        public int MaxEnergy { get; set; }
        public int Defense { get; set; }
        public int Strength { get; set; }
        public int Agility { get; set; }
        public int Charisma { get; set; }
        public int Willpower { get; set; }
        public int Intelligence { get; set; }
        public string CharacterName { get; set; }
        public int CharacterTypeID { get; set; }
        public int SkillPoints { get; set; }
        public int Level { get; set; }
        public int Gold { get; set; }
        public int Experience { get; set; }

        public List<CharacterAbilityModel> Abilities { get; set; }
        public List<CharacterInventoryModel> Inventory { get; set; }

        public string SaveMessage { get; set; }

        public CharacterModel()
        {
            Abilities = new List<CharacterAbilityModel>();
            Inventory = new List<CharacterInventoryModel>();
        }
        public CharacterModel(Character character) : this()
        {
            if (character != null)
            {
                CharacterID = character.CharacterID;
                CharacterName = character.CharacterName;
                CharacterTypeID = character.CharacterTypeID;
                Level = character.Level;
                MaxHealth = character.MaxHealth;
                MaxEnergy = character.MaxEnergy;

                Strength = character.Strength;
                Defense = character.Defense;
                Agility = character.Agility;
                Charisma = character.Charisma;
                Willpower = character.Willpower;
                Intelligence = character.Intelligence;
                SkillPoints = character.SkillPoints;

                foreach(var characterTypeAbility in character.CharacterType.Abilities)
                {
                    Abilities.Add(new CharacterAbilityModel(characterTypeAbility));
                }

                foreach(var ability in character.CharacterAbilities)
                {
                    var charAbility = Abilities.FirstOrDefault(a => a.AbilityID == ability.AbilityID);
                    if (charAbility != null)
                        charAbility.IsSelected = true;
                }

                foreach(var inventoryItem in character.CharacterInventories)
                {
                    Inventory.Add(new CharacterInventoryModel(inventoryItem));
                }
            }
        }
    }

    public class CharacterAbilityModel
    {
        public CharacterAbilityModel()
        {
        }
        
        public CharacterAbilityModel(Ability characterTypeAbility)
        {
            IsSelected = false;
            AbilityID = characterTypeAbility.AbilityID;
            Description = characterTypeAbility.Name;
        }

        public bool IsSelected { get; set; }
        public int AbilityID { get; set; }
        public string Description { get; set; }
    }

    public class CharacterInventoryModel
    {
        public CharacterInventoryModel()
        {

        }

        public CharacterInventoryModel(CharacterInventory characterInventory)
        {
            Description = characterInventory.Item.Name;
            Quantity = characterInventory.Quantity;
        }

        public int ItemID { get; set; }
        public string Description { get; set; }
        public int Quantity { get; set; }
    }
}