﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GameWeb.Models
{
    public class PlayerModel
    {
        public string PlayerName { get; set; }
        public List<string> PlayerWarehouse { get; set; }

        public PlayerModel()
        {
        }

        public PlayerModel(Player player) : this()
        {
            PlayerWarehouse = new List<string>();

            PlayerName = player.AspNetUser.Email;
        }
    }
}