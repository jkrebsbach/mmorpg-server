//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GameWeb
{
    using System;
    using System.Collections.Generic;
    
    public partial class Ability
    {
        public Ability()
        {
            this.CharacterAbilities = new HashSet<CharacterAbility>();
        }
    
        public int AbilityID { get; set; }
        public string Name { get; set; }
        public int Damage { get; set; }
        public int Cost { get; set; }
        public int Cooldown { get; set; }
        public bool Active { get; set; }
        public int CharacterTypeID { get; set; }
    
        public virtual ICollection<CharacterAbility> CharacterAbilities { get; set; }
        public virtual CharacterType CharacterType { get; set; }
    }
}
