﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(GameWeb.Startup))]
namespace GameWeb
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
