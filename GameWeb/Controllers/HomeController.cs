﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Microsoft.AspNet.Identity.Owin;
using System.Threading.Tasks;
using GameWeb.Models;

namespace GameWeb.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        [AllowAnonymous]
        public async Task<ActionResult> Index()
        {
            using (skyfishEntities entities = new skyfishEntities())
            {
                var userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
                var user = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);

                if (user != null)
                {
                    Player player = entities.Players.FirstOrDefault(p => p.UserID == user.Id);

                    if (player == null)
                    {
                        player = new Player()
                        {
                            UserID = user.Id
                        };

                        entities.Players.Add(player);
                        entities.SaveChanges();
                    }

                    return View("Player", new Models.PlayerModel(player));
                }
            }

            return View();
        }

        public async Task<ActionResult> Player()
        {
            ViewBag.Message = "Your application description page.";

            Player player = null;

            var userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var user = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);

            if (user != null)
            {
                using (skyfishEntities entities = new skyfishEntities())
                {
                    player = entities.Players.FirstOrDefault(p => p.UserID == user.Id);
                }
            }

            return View(new Models.PlayerModel(player));
        }

        public async Task<ActionResult> Character()
        {
            Character character = null;

            var userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var user = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);

            if (user != null)
            {
                using (skyfishEntities entities = new skyfishEntities())
                {
                    character = entities.Characters.Include("CharacterAbilities").Include("CharacterAbilities.Ability")
                        .Include("CharacterInventories").Include("CharacterInventories.Item")
                        .Include("CharacterType").Include("CharacterType.Abilities")
                        .FirstOrDefault(p => p.UserID == user.Id);
                }
            }

            return View(new Models.CharacterModel(character));
        }

        public async Task<ActionResult> ResetCharacter(int characterID)
        {
            var userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var user = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);

            CharacterModel newStats = new CharacterModel();
            
            if (user != null)
            {
                using (skyfishEntities entities = new skyfishEntities())
                {
                    var character = entities.Characters.FirstOrDefault(p => p.UserID == user.Id && p.CharacterID == characterID);

                    if (character != null)
                    {
                        newStats = CharacterModel.InitializeCharacter(character.CharacterTypeID);
                        newStats.SaveMessage = "Character reset successfully.";
                        newStats.CharacterName = character.CharacterName;

                        character.MaxHealth = newStats.MaxHealth;
                        character.MaxEnergy = newStats.MaxEnergy;
                        character.Defense = newStats.Defense;
                        character.Strength = newStats.Strength;
                        character.Agility = newStats.Agility;
                        character.Charisma = newStats.Charisma;
                        character.Willpower = newStats.Willpower;
                        character.Intelligence = newStats.Intelligence;
                        character.SkillPoints = newStats.SkillPoints;

                        character.EquipBody = null;
                        character.EquipHead = null;
                        character.EquipLeftHand = null;
                        character.EquipRightHand = null;

                        character.Gold = 0;
                        character.Experience = 0;
                        character.Level = 1;

                        var ability = character.CharacterAbilities.FirstOrDefault();
                        while (ability != null)
                        {
                            entities.CharacterAbilities.Remove(ability);
                            ability = character.CharacterAbilities.FirstOrDefault();
                        }

                        var oldInventory = character.CharacterInventories.FirstOrDefault();
                        while (oldInventory != null)
                        {
                            entities.CharacterInventories.Remove(oldInventory);
                            oldInventory = character.CharacterInventories.FirstOrDefault();
                        }

                        var qkc = character.QuestKillCounts.FirstOrDefault();
                        while (qkc != null)
                        {
                            entities.QuestKillCounts.Remove(qkc);
                            qkc = character.QuestKillCounts.FirstOrDefault();
                        }

                        var qtc = character.QuestTalkCounts.FirstOrDefault(); 
                        while (qtc != null)
                        {
                            entities.QuestTalkCounts.Remove(qtc);
                            qtc = character.QuestTalkCounts.FirstOrDefault(); 
                        }

                        var cq = character.CharacterQuests.FirstOrDefault();
                        while (cq != null)
                        {
                            entities.CharacterQuests.Remove(cq);
                            cq = character.CharacterQuests.FirstOrDefault();
                        }

                        foreach (var characterTypeAbility in character.CharacterType.Abilities)
                        {
                            newStats.Abilities.Add(new CharacterAbilityModel(characterTypeAbility));
                        }

                        entities.SaveChanges();

                        // Initial inventory
                        foreach (var newInventory in newStats.Inventory)
                        {
                            character.CharacterInventories.Add(
                                new CharacterInventory()
                                {
                                    ItemID = newInventory.ItemID,
                                    Quantity = newInventory.Quantity
                                });
                        }

                        entities.SaveChanges();
                    }
                }
            }

            return View("Character", newStats);
        }

        [HttpPost]
        public async Task<ActionResult> Character(CharacterModel model)
        {
            var userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var user = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);

            if (user != null)
            {
                using (skyfishEntities entities = new skyfishEntities())
                {
                    var character = entities.Characters.FirstOrDefault(p => p.UserID == user.Id && p.CharacterID == model.CharacterID);

                    if (character != null)
                    {
                        if (character.CharacterName != model.CharacterName && entities.Characters.Any(c => c.CharacterName == model.CharacterName))
                        {
                            model.SaveMessage = "Character name not available";
                        }
                        else
                        {
                            character.CharacterName = model.CharacterName;
                            character.CharacterTypeID = model.CharacterTypeID;

                            entities.SaveChanges();

                            model = new CharacterModel(character);

                            model.SaveMessage = "Character updated succefully";
                        }
                    }
                    else
                    {
                        model.SaveMessage = "Unexpected error - unable to find character";
                    }
                }
            }

            return View(model);
        }

        [AllowAnonymous]
        public ActionResult About()
        {
            ViewBag.Message = "New Moon Legacy";

            return View();
        }

        [AllowAnonymous]
        public ActionResult Contact()
        {
            ViewBag.Message = "New Moon Legacy";

            return View();
        }

        [AllowAnonymous]
        public ActionResult Demo()
        {
            ViewBag.Message = "Demo.";

            return View();
        }

        [AllowAnonymous]
        public ActionResult AgeOfCronus()
        {
            ViewBag.Message = "Age Of Cronus.";

            return View();
        }
    }
}